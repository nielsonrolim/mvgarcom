//
//  PedidoViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "Pedido.h"

@class AppDelegate;
@class MenuViewController;

@interface PedidoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, RKRequestDelegate>

@property (nonatomic, strong) IBOutlet UILabel* lblMesa;
@property (nonatomic, strong) IBOutlet UILabel* lblSubTotal;
@property (nonatomic, strong) IBOutlet UITableView* tbvItemsPedido;
@property (nonatomic, strong) IBOutlet UIButton* btnRemoverItem;
@property (nonatomic, strong) IBOutlet UIButton* btnEnviaPedido;
@property (nonatomic, strong) IBOutlet UIView* viewCarregando;
@property (nonatomic, assign) BOOL wsAtivo;
@property (nonatomic, strong) MenuViewController* menuVC;
@property (nonatomic, strong) Pedido* pedido;
@property (nonatomic, strong) IBOutlet UILabel* lblMsgAguarde1;
@property (nonatomic, strong) AppDelegate* appDelegate;

- (IBAction) enviaPedido: (id) sender;
- (IBAction) adicionaItem:(id) sender;
- (IBAction) editaItem: (id) sender;
- (NSDecimalNumber*) calculaSubtotal;

- (void) alertaPedidoEnviadoFalhaImpressao;
- (void) alertaPedidoNaoVazio;

@end
