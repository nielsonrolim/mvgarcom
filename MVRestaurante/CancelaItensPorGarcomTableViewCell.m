//
//  CancelaItensPorGarcomTableViewCell.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CancelaItensPorGarcomTableViewCell.h"
#import "ItemCancelamento.h"
#import "Funcoes.h"
#import "CancelaItensPorGarcomViewController.h"

@implementation CancelaItensPorGarcomTableViewCell

@synthesize item = _item;
@synthesize nome = _nome;
@synthesize btnAdicionaQuantidade = _btnAdicionaQuantidade;
@synthesize btnSubtraiQuantidade = _btnSubtraiQuantidade;
@synthesize lblCompl = _lblCompl;
@synthesize lblObs = _lblObs;
@synthesize lblQuantidade = _lblQuantidade;
@synthesize lblQuantidadeACancelar = _lblQuantidadeACancelar;
@synthesize lblNomeGarcom = _lblNomeGarcom;
@synthesize cancelaItensPorGarcomVC = _cancelaItensPorGarcomVC;
@synthesize linha = _linha;

- (NSUInteger) linha {
    if (!_linha) {
        _linha = 0;
    }
    return _linha;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction) adicionaQuantidade: (id) sender {
	self.item.quantidadeACancelar = [NSNumber numberWithInt:[self.item.quantidadeACancelar intValue] + 1];
    if ([self.item.quantidadeACancelar intValue] > [self.item.quantidade intValue]) {
        self.item.quantidadeACancelar = self.item.quantidade;
    }
	[self reloadData];
    self.cancelaItensPorGarcomVC.celulaSelecionada = self.linha;
    [self.cancelaItensPorGarcomVC.tbvCancelaPorGarcom reloadData];
}

- (IBAction) subtraiQuantidade: (id) sender {
	self.item.quantidadeACancelar = [NSNumber numberWithInt:[self.item.quantidadeACancelar intValue] - 1];
	if ([self.item.quantidadeACancelar intValue] < 1) {
		self.item.quantidadeACancelar = [NSNumber numberWithInt:0];
	}
	[self reloadData];
}

- (void) reloadData {
	if (self.item) {
		Produto* produto = self.item.produto;
        Garcon* garcom = self.item.garcom;
                
		self.nome.text = produto.nome;
        self.lblNomeGarcom.text = garcom.nome;
        self.lblQuantidade.text = [self.item.quantidade stringValue];
		self.lblQuantidadeACancelar.text = [self.item.quantidadeACancelar stringValue];
		
        self.lblObs.text = [self.item observacoesToString];
        self.lblObs.numberOfLines = 0;
        [self.lblObs sizeToFit];
		
        self.lblCompl.text = [self.item complementosToString];
        self.lblCompl.numberOfLines = 0;
        [self.lblCompl sizeToFit];
	}
}

@end
