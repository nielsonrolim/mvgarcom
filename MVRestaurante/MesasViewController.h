//
//  MesasViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class EscolherMesaViewController;

@interface MesasViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) EscolherMesaViewController* escolherMesaVC;
@property (nonatomic, strong) NSArray* mesas;
@property (nonatomic, strong) AppDelegate* appDelegate;

- (IBAction) voltar: (id) sender;
- (id)initWithTitle:(NSString*) titulo;

@end
