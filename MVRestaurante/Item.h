//
//  Item.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Produto.h"

@interface Item : NSObject

@property (nonatomic, strong) Produto* produto;
@property (nonatomic, strong) NSNumber* quantidade;
@property (nonatomic, strong) NSMutableArray* observacoesSelecionadas;
@property (nonatomic, strong) NSMutableArray* complementosSelecionados;
@property (nonatomic, strong) NSString* outrasObservacoes;

- (NSDecimalNumber*) calculaPreco;
- (NSDecimalNumber*) calculaSubtotal;
- (NSString*) observacoesToString;
- (NSString*) complementosToString;

@end
