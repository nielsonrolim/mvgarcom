//
//  CancelaItensViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "Conta.h"
#import "AppDelegate.h"

@class PedidoViewController;

@interface CancelaItensViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, RKRequestDelegate>

@property (nonatomic, strong) Conta* conta;
@property (nonatomic, strong) PedidoViewController* pedidoViewController;
@property (nonatomic, strong) IBOutlet UILabel* lblMesa;
@property (nonatomic, strong) IBOutlet UITableView* tbvConta;
@property (nonatomic, strong) IBOutlet UILabel* lblSubtotal;
@property (nonatomic, strong) IBOutlet UILabel* lblTaxaServico;
@property (nonatomic, strong) IBOutlet UILabel* lblTotal;
@property (nonatomic, assign) BOOL wsAtivo;
@property (nonatomic, strong) IBOutlet UIView* viewCarregando;
@property (nonatomic, strong) IBOutlet UILabel* lblMsgAguarde;
@property (nonatomic, strong) AppDelegate* appDelegate;


@end
