//
//  ItemCancelamento.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ItemCancelamento.h"

@implementation ItemCancelamento

@synthesize garcom = _garcom;
@synthesize quantidadeACancelar = _quantidadeACancelar;


- (NSNumber*) quantidadeACancelar {
    if (!_quantidadeACancelar) {
        _quantidadeACancelar = [[NSNumber alloc] initWithInt:0];
    }
    return _quantidadeACancelar;
}

@end
