//
//  InicioViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InicioViewController.h"
#import "ConfiguracaoViewController.h"
#import "LoginViewController.h"
#import "SMXMLDocument.h"

@implementation InicioViewController

@synthesize wsAtivo = _wsAtivo;
@synthesize lblVersao = _lblVersao;
@synthesize btnIniciar = _btnIniciar;
@synthesize viewCarregando = _viewCarregando;


#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.wsAtivo = NO;
    
    self.lblVersao.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.viewCarregando.hidden = YES;
    self.btnIniciar.enabled = YES;
}

- (void)viewDidUnload
{
    [self setLblVersao:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Alerts

- (void) alertaProblemaConexao {
	NSString* msg_alert = [NSString stringWithString:@"Não foi possível conectar ao servidor."];
	
	UIAlertView *alert = [[UIAlertView alloc] init];
	alert.title = @"Problema de conexão";
	alert.message = msg_alert;
	alert.delegate = self;
	alert.tag = 1;
	[alert addButtonWithTitle:@"Tentar novamente"];
	[alert addButtonWithTitle:@"Cancelar"];
    [alert addButtonWithTitle:@"Sair da aplicação"];
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (alertView.tag == 1) {
		if (buttonIndex == 0) {
			[self iniciarAplicacao:nil];
		} else if (buttonIndex == 1) {
            self.viewCarregando.hidden = YES;
            self.btnIniciar.enabled = YES;
            //            [self abreConfiguracao:nil];
		} else if (buttonIndex == 2) {
			exit(0);
		}
	}
}

#pragma mark - RKRequestDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
    if ([response isNotFound]) {
        [self alertaProblemaConexao];
    }
    
	if ([request isGET]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
			
			if ([[document.root value] isEqualToString:@"ATIVO"]) {
				self.wsAtivo = YES;
                [self iniciaApp];
			}	
		}
	}
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	NSLog(@"INATIVO... Falhou");
    NSLog(@"%@", [error localizedDescription]);
	self.wsAtivo = NO;
	[self alertaProblemaConexao];
}


#pragma mark - Metodos de inicilizacao

- (void) iniciaAppSeWsAtivo {
	[[RKClient sharedClient] get:@"/ativo" delegate:self];
}

- (void) iniciaApp {
    if (self.wsAtivo) {
        LoginViewController* loginVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        UINavigationController* loginNavCon = [[UINavigationController alloc] initWithRootViewController:loginVC];
        [self presentModalViewController:loginNavCon animated:YES];
    }
}

#pragma mark - IBActions


- (IBAction) iniciarAplicacao:(id)sender {
    
    self.viewCarregando.hidden = NO;
    self.btnIniciar.enabled = NO;
    
    if ([[RKClient sharedClient] isNetworkAvailable]) {
        [self iniciaAppSeWsAtivo];
    } else {
        [self alertaProblemaConexao];
    }
    
    NSLog(@"serverURL: %@", [[RKClient sharedClient] baseURL]);
}

- (IBAction) abreConfiguracao:(id)sender {
    ConfiguracaoViewController* configuracaoVC = [[ConfiguracaoViewController alloc] initWithNibName:@"ConfiguracaoViewController" bundle:nil];
	UINavigationController *configuracaoNavCon = [[UINavigationController alloc] initWithRootViewController:configuracaoVC];
	[self presentModalViewController:configuracaoNavCon animated:YES];
}


#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)showInfo:(id)sender
{    
    FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
    controller.delegate = self;
    UINavigationController *controllerNavCon = [[UINavigationController alloc] initWithRootViewController:controller];
    controllerNavCon.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controllerNavCon animated:YES];
}

@end
