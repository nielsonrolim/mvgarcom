//
//  DropDownCell.m
//  DropDownTest
//
//  Created by Florian Krüger on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DropDownCell.h"


@implementation DropDownCell

@synthesize textLabel;
@synthesize arrow_up = _arrow_up;
@synthesize arrow_down = _arrow_down;
@synthesize isOpen = _isOpen;

- (void) setOpen 
{
    [self.arrow_down setHidden:YES];
    [self.arrow_up setHidden:NO];
    self.isOpen = YES;
}

- (void) setClosed
{
    [self.arrow_down setHidden:NO];
    [self.arrow_up setHidden:YES];
    [self setIsOpen:NO];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
