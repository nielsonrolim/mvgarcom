//
//  ConfiguracaoViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "AppDelegate.h"
#import "ConfiguracaoViewController.h"
#import "ConstantesConf.h"

@implementation ConfiguracaoViewController

@synthesize configuracao = _configuracao;
@synthesize txtIP = _txtIP;
@synthesize txtPorta = _txtPorta;
@synthesize swtPesquisaCategoria = _swtPesquisaCategoria;
@synthesize swtModoDemonstracao = _swtModoDemonstracao;
@synthesize swtMesasPorSetor = _swtMesasPorSetor;
@synthesize swtBuscaComplementosPorNome = _swtBuscaComplementosPorNome;
@synthesize swtBuscaObservacoesPorNome = _swtBuscaObservacoesPorNome;
@synthesize swtModoComanda = _swtModoComanda;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (IBAction) textFieldDoneEditing: (id) sender {
	[self.txtIP resignFirstResponder];
    [self.txtPorta resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self textFieldDoneEditing:nil];
    return YES;
}

- (IBAction) salvar:(id)sender {
    
    if (self.txtIP.text != nil) {
        [self.configuracao setObject:self.txtIP.text forKey:ENDERECO_IP_KEY];
    }
    
    if (self.txtPorta.text == nil || [self.txtPorta.text isEqualToString:@""]) {
        self.txtPorta.text = @"80";
    }
    [self.configuracao setObject:self.txtPorta.text forKey:PORTA_KEY];
    
    if (self.swtPesquisaCategoria.on) {
        [self.configuracao setObject:@"YES" forKey:PESQUISAPORCATEGORIA_KEY];
    } else {
        [self.configuracao setObject:@"NO" forKey:PESQUISAPORCATEGORIA_KEY];
    }

    if (self.swtBuscaComplementosPorNome.on) {
        [self.configuracao setObject:@"YES" forKey:BUSCACOMPLEMENTOSPORNOME_KEY];
    } else {
        [self.configuracao setObject:@"NO" forKey:BUSCACOMPLEMENTOSPORNOME_KEY];
    }

    if (self.swtBuscaObservacoesPorNome.on) {
        [self.configuracao setObject:@"YES" forKey:BUSCAOBSERVACOESPORNOME_KEY];
    } else {
        [self.configuracao setObject:@"NO" forKey:BUSCAOBSERVACOESPORNOME_KEY];
    }

    if (self.swtMesasPorSetor.on) {
        [self.configuracao setObject:@"YES" forKey:MESASPORSETOR_KEY];
    } else {
        [self.configuracao setObject:@"NO" forKey:MESASPORSETOR_KEY];
    }

    if (self.swtModoComanda.on) {
        [self.configuracao setObject:@"YES" forKey:MODOCOMANDA_KEY];
    } else {
        [self.configuracao setObject:@"NO" forKey:MODOCOMANDA_KEY];
    }

    RKClient* client = [RKClient sharedClient];
    RKObjectManager* objectManager = [RKObjectManager sharedManager];
        
    if (self.swtModoDemonstracao.on) {
        [self.configuracao setObject:@"YES" forKey:MODODEMONSTRACAO_KEY];
        [client setBaseURL:[NSString stringWithFormat:@"http://%@:%@/mvarandas/mvws.exe", DEMOENDERECO, DEMOPORTA]];
    } else {
        [self.configuracao setObject:@"NO" forKey:MODODEMONSTRACAO_KEY];
        [client setBaseURL:[NSString stringWithFormat:@"http://%@:%@/mvarandas/mvws.exe", self.txtIP.text, self.txtPorta.text]];
    }
    [objectManager setClient:client];

    
    [self.configuracao writeToURL:self.appDelegate.configuracaoURL atomically:YES];
    
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction) cancelar:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (void) disableTxtIPAndTxtPorta {
    self.txtIP.enabled = NO;
    self.txtIP.alpha = 0.4;
    
    self.txtPorta.enabled = NO;
    self.txtPorta.alpha = 0.4;    
}

- (void) enableTxtIpAndTxtPorta {
    self.txtIP.enabled = YES;
    self.txtIP.alpha = 1.0;
    
    self.txtPorta.enabled = YES;
    self.txtPorta.alpha = 1.0;
}

- (IBAction) swtModoDemonstracaoAlterado:(id)sender {
    UISwitch* swt = (UISwitch*) sender;
    if (swt.on) {
        [self disableTxtIPAndTxtPorta];
    } else {
        [self enableTxtIpAndTxtPorta];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = [NSString stringWithString:@"Configuração"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(cancelar:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Salvar" style:UIBarButtonItemStylePlain target:self action:@selector(salvar:)];
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL* storeURL = [[appDelegate applicationDocumentsDirectory] URLByAppendingPathComponent:@"ConfiguracaoMVRestaurante.plist"];
    NSMutableDictionary* confTmp = [[NSMutableDictionary alloc] initWithContentsOfURL:storeURL];
    if (confTmp == nil) {
        confTmp = [[NSMutableDictionary alloc] init];
    }
    
    self.configuracao = confTmp;
    
    self.txtIP.text = [self.configuracao objectForKey:ENDERECO_IP_KEY];
    self.txtPorta.text = [self.configuracao objectForKey:PORTA_KEY];
    
    if ([[self.configuracao objectForKey:PESQUISAPORCATEGORIA_KEY] isEqualToString:@"NO"]) {
        self.swtPesquisaCategoria.on = NO;
    } else {
        self.swtPesquisaCategoria.on = YES;
    }

    if ([[self.configuracao objectForKey:BUSCACOMPLEMENTOSPORNOME_KEY] isEqualToString:@"NO"]) {
        self.swtBuscaComplementosPorNome.on = NO;
    } else {
        self.swtBuscaComplementosPorNome.on = YES;
    }

    if ([[self.configuracao objectForKey:BUSCAOBSERVACOESPORNOME_KEY] isEqualToString:@"NO"]) {
        self.swtBuscaObservacoesPorNome.on = NO;
    } else {
        self.swtBuscaObservacoesPorNome.on = YES;
    }

    if ([[self.configuracao objectForKey:MESASPORSETOR_KEY] isEqualToString:@"YES"] || [self.configuracao count] == 0) {
        self.swtMesasPorSetor.on = YES;
    } else {
        self.swtMesasPorSetor.on = NO;
    }
    
    if ([[self.configuracao objectForKey:MODODEMONSTRACAO_KEY] isEqualToString:@"YES"] || [self.configuracao count] == 0) {
        self.swtModoDemonstracao.on = YES;
        [self disableTxtIPAndTxtPorta];
    } else {
        self.swtModoDemonstracao.on = NO;
        [self enableTxtIpAndTxtPorta];
    }

    if ([[self.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"] || [self.configuracao count] == 0) {
        self.swtModoComanda.on = YES;
    } else {
        self.swtModoComanda.on = NO;
    }
}

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}




@end
