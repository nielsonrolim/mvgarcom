//
//  Cancelamento.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Cancelamento.h"

@implementation Cancelamento

@synthesize itemConta = _itemConta;
@synthesize items = _items;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (NSMutableArray*) items {
    if (_items == nil) {
        _items = [[NSMutableArray alloc] init];
    }
    return _items;
}

- (BOOL) estaVazio {
    return [self.items count] == 0;
}


@end
