//
//  AppDelegate.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "Garcon.h"

@class InicioViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) Garcon* operador;
@property (nonatomic, strong) NSURL* configuracaoURL;
@property (nonatomic, strong) NSMutableDictionary* configuracao;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (RKObjectManager*) configuraWebServiceComIp: (NSString*) enderecoIP porta: (NSString*) porta;

@property (strong, nonatomic) InicioViewController *inicioViewController;

@end
