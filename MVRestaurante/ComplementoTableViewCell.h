//
//  ComplementoTableViewCellCell.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Produto.h"

@class ComplementosViewController;

@interface ComplementoTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblNome;
@property (strong, nonatomic) IBOutlet UILabel *lblDetalhes;
@property (strong, nonatomic) IBOutlet UILabel *lblQuantidade;
@property (strong, nonatomic) ComplementosViewController* complementosVC;
@property (strong, nonatomic) Produto* complemento;

- (IBAction)btnAdicionarComplementoPressionado:(UIButton *)sender;
- (IBAction)btnRemoverComplementoPressionado:(UIButton *)sender;

@end
