//
//  CancelaItensViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CancelaItensViewController.h"
#import "SMXMLDocument.h"
#import "ItemConta.h"
#import "Obs.h"
#import "CancelaItensTableViewCell.h"
#import "PedidoViewController.h"
#import "CancelaItensPorGarcomViewController.h"
#import "CustomAlert.h"
#import "Funcoes.h"
#import "ConstantesConf.h"

@implementation CancelaItensViewController

@synthesize lblMesa = _lblMesa;
@synthesize tbvConta = _tbvConta;
@synthesize lblSubtotal = _lblSubtotal;
@synthesize lblTaxaServico = _lblTaxaServico;
@synthesize lblTotal = _lblTotal;
@synthesize wsAtivo = _wsAtivo;
@synthesize conta = _conta;
@synthesize pedidoViewController = _pedidoViewController;
@synthesize viewCarregando = _viewCarregando;
@synthesize lblMsgAguarde = _lblMsgAguarde;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (Conta*) conta {
    if (!_conta) {
        _conta = [[Conta alloc] init];
    }
    return _conta;
}

#pragma mark - alertas

- (void) alertaProblemaEnvioPedido:(NSString*) msgAlerta; {
    if ([msgAlerta isEqualToString:@""] || msgAlerta == nil) {
        msgAlerta = [NSString stringWithString:@"Houve um problema ao tentar enviar o pedido. Tente novamente."];
    }
	CustomAlert *alertView = [[CustomAlert alloc] initWithTitle:@"Problema no envio"
														message:msgAlerta
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [CustomAlert setBackgroundColor:[UIColor redColor] 
                    withStrokeColor:[UIColor whiteColor]];
	[alertView show];
}

- (void) alertaContaImpressa {
    NSString* msg_alert = [NSString stringWithFormat:@"A conta da mesa %@ foi impressa com sucesso.", self.conta.mesa];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Conta impressa"
                                                        message:msg_alert
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void) alertaVermelhoComTitulo:(NSString*)titulo mensagem:(NSString*)msg {
    if ([titulo isEqualToString:@""] || titulo == nil) {
        titulo = @"Não Impresso";
    }
    if ([msg isEqualToString:@""] || msg == nil) {
        msg = @"Houve um problema ao tentar imprimir a conta. Tente novamente.";
    }
	CustomAlert *alertView = [[CustomAlert alloc] initWithTitle:titulo
														message:msg
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [CustomAlert setBackgroundColor:[UIColor redColor] 
                    withStrokeColor:[UIColor whiteColor]];
	[alertView show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{ 
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
        }        
    }
}

- (void) alertaProblemaCancelaItens {
	NSString* msg_alert = [NSString stringWithString:@"Houve um problema ao tentar cancelar itens. Tente novamente."];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Problema no cancelamento"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}

- (void) novaMesa: (id) sender {
    if ([self.pedidoViewController.pedido estaVazio]) {
        [self dismissModalViewControllerAnimated:YES];
    } else {
        [self.pedidoViewController alertaPedidoNaoVazio];
    }
}

- (void) cancelaItensSeWsAtivo {
	[[RKClient sharedClient] get:@"/ativo" delegate:self];
}

- (IBAction) cancelaItens: (id) sender {
    self.viewCarregando.hidden = NO;
    
    if ([[RKClient sharedClient] isNetworkAvailable]) {
        [self cancelaItensSeWsAtivo];
    } else {
        //        self.viewCarregando.hidden = YES;
        [self alertaProblemaCancelaItens];
    }
}

- (void) executaCancelaItens {
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSString* titulo;
        if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
            titulo = @"Comanda";
        } else {
            titulo = @"Mesa";
        }
        
        // Custom initialization
        self.title = @"Cancela Itens";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Nova %@", titulo] style:UIBarButtonItemStylePlain target:self action:@selector(novaMesa:)];
		
		UIImage* anImage = [UIImage imageNamed:@"delete.png"];
		UITabBarItem* theItem = [[UITabBarItem alloc] initWithTitle:@"Cancela Itens" image:anImage tag:0];
		self.tabBarItem = theItem;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"Comanda";
    } else {
        titulo = @"Mesa";
    }

    // Do any additional setup after loading the view from its nib.
    self.lblMesa.text = [NSString stringWithFormat:@"%@ %@", titulo, self.conta.mesa];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
    self.lblMsgAguarde.text = @"Carregando conta";
    self.viewCarregando.hidden = NO;
    
	NSDictionary* params = [NSDictionary dictionaryWithObject:self.conta.mesa forKey:@"MESA"];
	[[RKClient sharedClient] post:@"/conta" params:params delegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	self.tabBarItem.badgeValue = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.conta.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	ItemConta* itemConta = (ItemConta*)[self.conta.items objectAtIndex:indexPath.row];
	
	if (([itemConta.complementosSelecionados count] > 0) || ([itemConta.observacoesSelecionadas count] > 0)) {
		CGFloat altura;
		altura = 105;
		int linhasAdicionais = [itemConta.complementosSelecionados count] / 5;
		for (int i=0; i < linhasAdicionais; i++) {
			altura += 70;
		}
		return altura;
	} else {
		return 55;
	}
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	ItemConta* itemConta = (ItemConta*)[self.conta.items objectAtIndex:indexPath.row];
	BOOL temComplementos = NO;
	BOOL temObservacoes = NO;
	
	if ([itemConta.observacoesSelecionadas count] > 0) {
		temObservacoes = YES;
	}
	
	if ([itemConta.complementosSelecionados count] > 0) {
		temComplementos = YES;
	}
	
    static NSString* CellIdentifier = @"cancelaItemCell";
	
	CancelaItensTableViewCell* cell = (CancelaItensTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	NSArray* topLevelObjects = [NSArray array];
	
	if (temObservacoes || temComplementos) {
		if (temObservacoes && temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensComplObsTableViewCell" owner:nil options:nil];
		} else if (temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensComplTableViewCell" owner:nil options:nil];
		} else if (temObservacoes) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensObsTableViewCell" owner:nil options:nil];
		}
		
	} else {
		topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensTableViewCell" owner:nil options:nil];
	}
	
    
	for(id currentObject in topLevelObjects) {			
		if([currentObject isKindOfClass:[CancelaItensTableViewCell class]]) {
			cell = (CancelaItensTableViewCell *)currentObject;
			break;
		}
	}
	
	cell.item = itemConta;
    cell.lblQuantidade.hidden = NO;
    //	cell.lblQuantidadeACancelar.hidden = NO;
    //	cell.btnAdicionaQuantidade.hidden = NO;
    //	cell.btnSubtraiQuantidade.hidden = NO;
	[cell reloadData];
	
	return cell;
    
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    ItemConta* itemConta = (ItemConta*)[self.conta.items objectAtIndex:indexPath.row];
    CancelaItensPorGarcomViewController* cancelaPorGarcomVC = [[CancelaItensPorGarcomViewController alloc] initWithNibName:@"CancelaItensPorGarcomViewController" bundle:nil];
    cancelaPorGarcomVC.cancelamento.itemConta = itemConta;
    [self.navigationController pushViewController:cancelaPorGarcomVC animated:YES];
}

#pragma mark - RKRequestDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
    
	if ([request isPOST]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
            
            if ([document.root.name isEqualToString:@"tbConta"]) {
                NSMutableArray* itensTmp = [[NSMutableArray alloc] init];
                
                for (SMXMLElement* itemXml in [document.root childrenNamed:@"conta"] ) {
                    
                    NSString* codigoObs = [itemXml valueWithPath:@"obs1"];
                    
                    if (codigoObs != nil) {
                        ItemConta* itemTmp = [[ItemConta alloc] init];
                        NSMutableArray* observacoesSelecionadasTmp = [[NSMutableArray alloc] init];
                        NSMutableArray* complementosSelecionadosTmp = [[NSMutableArray alloc] init];
                        
                        itemTmp.produto = [Produto findByCodigo:[itemXml valueWithPath:@"codigo"]];
                        itemTmp.quantidade = [NSNumber numberWithDouble:[[itemXml valueWithPath:@"quant"] doubleValue]];
                        
                        for (int i = 1; i <= 6; i++) {
                            codigoObs = [itemXml valueWithPath:[NSString stringWithFormat:@"obs%d", i]];
                            if (![codigoObs isEqualToString:@"0"]) {
                                [observacoesSelecionadasTmp addObject:[Obs findByCodigo:codigoObs AndCategoria:[itemTmp.produto.categ stringValue]]];
                            }
                        }
                        
                        NSString* codigoCompl;
                        for (int i = 1; i <= 6; i++) {
                            codigoCompl = [itemXml valueWithPath:[NSString stringWithFormat:@"parte%d", i]];
                            if (![codigoCompl isEqualToString:@"0"]) {
                                [complementosSelecionadosTmp addObject:[Produto findByCodigo:codigoCompl]];
                            }
                        }
                        
                        itemTmp.observacoesSelecionadas = observacoesSelecionadasTmp;
                        itemTmp.complementosSelecionados = complementosSelecionadosTmp;
                        itemTmp.outrasObservacoes = [itemXml valueWithPath:@"observacao"];
                        itemTmp.sequencia = [itemXml valueWithPath:@"seq"];
                        
                        itemTmp.conta = self.conta;
                        
                        [itensTmp addObject:itemTmp];
                        
                    }				
                }
                self.conta.items = itensTmp;
                
                //                self.lblSubtotal.text = [[NSString stringWithFormat:@"R$ %@", [document.root valueWithPath:@"subtotal"]] stringByReplacingOccurrencesOfString:@"." withString:@","];
                //                self.lblTaxaServico.text = [[NSString stringWithFormat:@"R$ %@", [document.root valueWithPath:@"taxa"]] stringByReplacingOccurrencesOfString:@"." withString:@","];
                //                self.lblTotal.text = [[NSString stringWithFormat:@"R$ %@", [document.root valueWithPath:@"total"]] stringByReplacingOccurrencesOfString:@"." withString:@","];
                
                [self.tbvConta reloadData];
                
                self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [self.conta.items count]];
            } else if ([document.root.value isEqualToString:@"IMPRESSO"]) {
                [self alertaContaImpressa];
            } else if ([document.root.value isEqualToString:@"CONTA FECHADA"]) {
                [self alertaVermelhoComTitulo:@"Conta Fechada" mensagem:nil];
            } else if ([document.root.value isEqualToString:@"CONTA ABERTA"]) {
                [self alertaVermelhoComTitulo:@"Mesa Aberta" mensagem:nil];
            } else if ([document.root.value isEqualToString:@"CONTA EMITIDA"]) {
                [self alertaVermelhoComTitulo:@"Conta Já Emitida" mensagem:nil];
            }
        }
        self.lblSubtotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.conta subtotal])];
        self.lblTaxaServico.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.conta taxaDeServico])];
        self.lblTotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.conta total])];
        self.viewCarregando.hidden = YES;
	}
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	NSLog(@"INATIVO... Falhou");
	self.wsAtivo = NO;
    self.viewCarregando.hidden = YES;
	[self alertaProblemaCancelaItens];
}

@end
