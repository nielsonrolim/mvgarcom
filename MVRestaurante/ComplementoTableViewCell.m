//
//  ComplementoTableViewCellCell.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ComplementoTableViewCell.h"
#import "ComplementosViewController.h"

@implementation ComplementoTableViewCell
@synthesize lblNome = _lblNome;
@synthesize lblDetalhes = _lblDetalhes;
@synthesize lblQuantidade = _lblQuantidade;
@synthesize complementosVC = _complementosVC;
@synthesize complemento = _complemento;

- (void) setComplemento:(Produto *)complemento {
    _complemento = complemento;
    self.lblNome.text = [complemento nome];
	NSMutableString *text = [NSMutableString stringWithFormat:@"Cód.: %@", [complemento codigo]];
    NSInteger tamProduto = [self.complementosVC.itemSelecionado.produto.tam intValue];
	if ([[complemento precoComplemento:tamProduto] doubleValue] > 0.0) {
        [text appendFormat:@"  |  Preço: R$ %@", [complemento precoComplemento:tamProduto]];
	}
    self.lblDetalhes.text = text;
    NSInteger quantidade = [self.complementosVC quantidadeDoComplemento:self.complemento];
    self.lblQuantidade.text = [NSString stringWithFormat:@"%d", quantidade];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnAdicionarComplementoPressionado:(UIButton *)sender {
    [self.complementosVC adicionaComplementoAosSelecionado:self.complemento];
    self.lblQuantidade.text = [NSString stringWithFormat:@"%d", [self.complementosVC quantidadeDoComplemento:self.complemento]];
}

- (IBAction)btnRemoverComplementoPressionado:(UIButton *)sender {
    [self.complementosVC removeComplementoDosSelecionados:self.complemento];
    self.lblQuantidade.text = [NSString stringWithFormat:@"%d", [self.complementosVC quantidadeDoComplemento:self.complemento]];
}


@end
