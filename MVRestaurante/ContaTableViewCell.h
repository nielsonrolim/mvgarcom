//
//  ContaTableViewCell.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemConta.h"


@interface ContaTableViewCell : UITableViewCell {
	IBOutlet UILabel* nome;
	IBOutlet UILabel* quantidade;
	IBOutlet UILabel* preco;
	IBOutlet UILabel* subtotal;
	IBOutlet UILabel* lblCompl;
	IBOutlet UILabel* lblObs;
	ItemConta* item;
}

@property (nonatomic, strong) IBOutlet UILabel* nome;
@property (nonatomic, strong) IBOutlet UILabel* quantidade;
@property (nonatomic, strong) IBOutlet UILabel* preco;
@property (nonatomic, strong) IBOutlet UILabel* subtotal;
@property (nonatomic, strong) IBOutlet UILabel* lblCompl;
@property (nonatomic, strong) IBOutlet UILabel* lblObs;
@property (nonatomic, strong) ItemConta* item;

- (void) reloadData;

@end
