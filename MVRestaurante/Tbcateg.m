//
//  Tbcateg.m
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Tbcateg.h"

@implementation Tbcateg

@synthesize identifier;
@synthesize fracao;
@synthesize metodo;
@synthesize c_complemento;
@synthesize nome;
@synthesize codigo;
@synthesize tipo;
@synthesize pede_observ;
@synthesize pede_complemento;
@synthesize e_complemento;
@synthesize peso;
@synthesize oculto;

@end
