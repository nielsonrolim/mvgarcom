//
//  DropDownCell.h
//  DropDownTest
//
//  Created by Florian Krüger on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DropDownCell : UITableViewCell

@property (nonatomic) BOOL isOpen;
@property (nonatomic, strong) IBOutlet UILabel *textLabel;
@property (nonatomic, strong) IBOutlet UIImageView *arrow_up;
@property (nonatomic, strong) IBOutlet UIImageView *arrow_down;

- (void) setOpen;
- (void) setClosed;

@end
