//
//  ContaTableViewCell.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ContaTableViewCell.h"
#import "Obs.h"
#import "Funcoes.h"


@implementation ContaTableViewCell

@synthesize nome;
@synthesize quantidade;
@synthesize preco;
@synthesize subtotal;
@synthesize item;
@synthesize lblObs;
@synthesize lblCompl;

- (void) reloadData {
	if (item) {
		Produto* produto = item.produto;
		self.nome.text = produto.nome;
		self.quantidade.text = [item.quantidade stringValue];
		self.preco.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.item calculaPreco])];
        self.subtotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.item calculaSubtotal])];
		
        if ([[item outrasObservacoes] isEqualToString:@""]) {
            lblObs.text = [item observacoesToString];
        } else {
            if ([item.observacoesSelecionadas count] > 0) {
                lblObs.text = [[item observacoesToString] stringByAppendingFormat:@", %@",[item outrasObservacoes]];
            } else {
                lblObs.text = [item outrasObservacoes];
            }
        }
        lblObs.numberOfLines = 0;
        [self.lblObs sizeToFit];
		
        lblCompl.text = [item complementosToString];
        lblCompl.numberOfLines = 0;
        [self.lblCompl sizeToFit];
	}
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}



@end
