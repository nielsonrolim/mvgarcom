//
//  MesasMapaViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 7/14/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import "MesasMapaViewController.h"
#import "EscolherMesaViewController.h"
#import "ConfiguracaoViewController.h"
#import "SMXMLDocument.h"
#import "Mesa.h"
#import "AppDelegate.h"
#import "ConstantesConf.h"

@implementation MesasMapaViewController

@synthesize scvPrincipal = _scvPrincipal;
@synthesize viewCarregando = _viewCarregando;
@synthesize escolherMesaVC = _escolherMesaVC;
@synthesize mesas = _mesas;
@synthesize lblDesenhadoMapa = _lblDesenhadoMapa;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (void) desenhaMapaMesas {
    
    int total_mesas = 99;
	int mesa_numero = 1;
	
	int margin_top = 10;
	int margin_left = 10;
	
	int botao_altura = 40;
	int botao_largura = 93;
	
	int num_colunas = 3;
	int num_linhas = ceil((float)total_mesas/num_colunas);
	int pos_x = margin_top;
	int pos_y = margin_left;
	int altura_total;
		
    //	NSMutableString* nomeMesa = [[[NSMutableString alloc] init] autorelease];
    NSMutableString* nomeMesa;
	
	for (int j = 0; j < num_linhas; j++) {
		
		if (j > 0) {
			pos_y += (botao_altura + margin_top);
		}
		
		for (int i = 0; i < num_colunas; i++) {
            
			
			if (i > 0) {
				pos_x += (botao_largura + margin_left);
 			}
			
			//create the button
			UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
			
			//set the position of the button
			button.frame = CGRectMake(pos_x, pos_y, botao_largura, botao_altura);
			
			//set the button's title
            if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
                nomeMesa = [NSMutableString stringWithFormat:@"Com. %d", mesa_numero];
            } else {
                nomeMesa = [NSMutableString stringWithFormat:@"Mesa %d", mesa_numero];
            }
			[button setTitle:nomeMesa forState:UIControlStateNormal];
			
			//listen for clicks
			[button addTarget:self action:@selector(buttonPressed:) 
			 forControlEvents:UIControlEventTouchUpInside];
            
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            for (Mesa* tauba in self.mesas) {
                if ([tauba.mesa intValue] == mesa_numero) {
                    if ([tauba.status isEqualToString:@"-1"]) {
                        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    } else if ([tauba.status isEqualToString:@"1"]) {
                        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                    }
                    break;
                }
            }
            
			//add the button to the view
			[self.scvPrincipal addSubview:button];
			
			total_mesas--;
			mesa_numero++;
            
			if (total_mesas == 0) {
				break;
			}			
			
		}
		
		pos_x = margin_left;
        
	}
	
	altura_total = pos_y + botao_altura + margin_top;
	
	self.scvPrincipal.contentSize = CGSizeMake(320,altura_total);

    self.viewCarregando.hidden = YES;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableDictionary* conf = self.appDelegate.configuracao;
    if ([[conf objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        self.lblDesenhadoMapa.text = @"Desenhando Mapa de Comandas";
        self.navigationItem.title = @"Mapa de Comandas";
    }
}


-(void)buttonPressed: (UIButton*) botao {
    NSString* mesa_numero;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        mesa_numero = [botao.currentTitle stringByReplacingOccurrencesOfString:@"Com. " withString:@""];
    } else {
        mesa_numero = [botao.currentTitle stringByReplacingOccurrencesOfString:@"Mesa " withString:@""];
    }
	self.escolherMesaVC.mesaEscolhida = mesa_numero;
	[self.navigationController popToRootViewControllerAnimated:YES];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.viewCarregando.hidden = NO;
//    [self carregaMapaMesas];
    [self performSelector:@selector(desenhaMapaMesas) withObject:nil afterDelay:1];
}

- (void)viewDidUnload {
    [self setLblDesenhadoMapa:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}



@end
