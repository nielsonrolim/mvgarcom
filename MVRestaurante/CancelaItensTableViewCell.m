//
//  CancelaItensTableViewCell.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CancelaItensTableViewCell.h"
#import "ItemConta.h"
#import "Obs.h"
#import "Funcoes.h"

@implementation CancelaItensTableViewCell

@synthesize item = _item;
@synthesize nome = _nome;
@synthesize preco = _preco;
@synthesize subtotal = _subtotal;
@synthesize lblCompl = _lblCompl;
@synthesize lblObs = _lblObs;
@synthesize lblQuantidade = _lblQuantidade;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) reloadData {
	if (self.item) {
		Produto* produto = self.item.produto;

		self.nome.text = produto.nome;
        self.lblQuantidade.text = [self.item.quantidade stringValue];
        self.preco.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula(self.item.produto.preco)];
        self.subtotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.item calculaSubtotal])];
		
        self.lblObs.text = [self.item observacoesToString];
        self.lblObs.numberOfLines = 0;
        [self.lblObs sizeToFit];
		
        self.lblCompl.text = [self.item complementosToString];
        self.lblCompl.numberOfLines = 0;
        [self.lblCompl sizeToFit];
	}
}

@end
