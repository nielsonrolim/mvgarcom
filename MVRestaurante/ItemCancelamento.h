//
//  ItemCancelamento.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"
#import "Garcon.h"

@interface ItemCancelamento : Item

@property (nonatomic, strong) Garcon* garcom;
@property (nonatomic, strong) NSNumber* quantidadeACancelar;


@end
