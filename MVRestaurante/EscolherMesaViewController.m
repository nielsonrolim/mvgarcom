//
//  EscolherMesaViewController.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 3/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EscolherMesaViewController.h"
#import "AppDelegate.h"
#import "SMXMLDocument.h"
#import "Mesa.h"
#import "Garcon.h"
#import "MesasMapaViewController.h"
#import "MesasViewController.h"
#import "MenuViewController.h"
#import "PedidoViewController.h"
#import "ContaViewController.h"
#import "CancelaItensViewController.h"
#import "ConstantesConf.h"


@implementation EscolherMesaViewController

@synthesize txtMesaEscolhida = _txtMesaEscolhida;
@synthesize viewCarregando = _viewCarregando;
@synthesize mesaEscolhida = _mesaEscolhida;
@synthesize mesas = _mesas;
@synthesize wsAtivo = _wsAtivo;
@synthesize selectorParaExecutar = _selectorParaExecutar;
@synthesize lblNomeDoGarcon = _lblNomeDoGarcon;
@synthesize lblCarregandoMesas = _lblCarregandoMesas;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (NSString*) mesaEscolhida {
    if (!_mesaEscolhida) {
        _mesaEscolhida = [NSString stringWithString:@""];
    }
    return _mesaEscolhida;
}

NSInteger comparaMesas(id arg1, id arg2, void *arg3) {
//    return [[(Mesa *)arg1 mesa] compare: [(Mesa *)arg2 mesa]];
    return [[(Mesa *)arg1 mesa] integerValue] > [[(Mesa *)arg2 mesa] integerValue];
	//Swap arg1 and arg2 to reverse the sort order
}

- (void) alertaMesaNaoEscolhida {
	NSString* msg_alert = [NSString stringWithString:@"É necessário escolher uma mesa para prosseguir."];
	
	UIAlertView *alert = [[UIAlertView alloc] init];
	alert.title = @"Mesa não escolhida";
	alert.message = msg_alert;
	alert.delegate = self;
	alert.tag = 1;
	[alert addButtonWithTitle:@"OK"];
	[alert show];
}

- (void) alertaProblemaConexao {
	NSString* msg_alert = [NSString stringWithString:@"Não foi possível conectar ao servidor e carregar o mapa de mesas."];
	
	UIAlertView *alert = [[UIAlertView alloc] init];
	alert.title = @"Problema de conexão";
	alert.message = msg_alert;
	alert.delegate = self;
	alert.tag = 2;
	[alert addButtonWithTitle:@"Tentar novamente"];
	[alert addButtonWithTitle:@"Configurar Servidor"];
    [alert addButtonWithTitle:@"Digitar mesa manualmente"];
    [alert addButtonWithTitle:@"Sair da aplicação"];
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        self.mesaEscolhida = @"";
    }
	if (alertView.tag == 2) {
		if (buttonIndex == 0) {
			[self carregaMapaMesas:self.selectorParaExecutar];
        } else if (buttonIndex == 1) {
            [self.navigationController popToRootViewControllerAnimated:YES];
		} else if (buttonIndex == 2) {
			exit(0);
		}		
	}
}

- (void) carregaMapaMesas:(SEL)aSelector {
    RKClient* rkClient = [RKClient sharedClient];
	
	if ([rkClient isNetworkAvailable]) {
        self.selectorParaExecutar = aSelector;
		[rkClient get:@"/mapa" delegate:self];
	} else {
		[self alertaProblemaConexao];
	}
	
}

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
    if ([response isNotFound]) {
        [self alertaProblemaConexao];
    }
    
	if ([request isGET]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
			
			if ([[document.root value] isEqualToString:@"ATIVO"]) {
				self.wsAtivo = YES;
			} else {
				NSMutableArray* mesasTmp = [[NSMutableArray alloc] init];
				
				for (SMXMLElement* mesaXml in [document.root childrenNamed:@"tbmapa"] ) {
					Mesa* tauba = [[Mesa alloc] init];
					tauba.mesa = [mesaXml valueWithPath:@"mesa"];
					tauba.status = [mesaXml valueWithPath:@"status"];
					[mesasTmp addObject:tauba];
				}
				
				self.mesas = [NSArray arrayWithArray:mesasTmp];
                
                if ([self respondsToSelector:self.selectorParaExecutar]) {
                    [self performSelector:self.selectorParaExecutar];
                }
			}
        }
    }
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	NSLog(@"INATIVO... Falhou");
	self.wsAtivo = NO;
	[self alertaProblemaConexao];
}


- (IBAction) abrirMesa: (id) sender {

//	PrincipalViewController* principalVC = [[PrincipalViewController alloc] initWithNibName:@"PrincipalViewController" bundle:nil];
//	principalVC.mesaEscolhida = txtMesaEscolhida.text;
//	txtMesaEscolhida.text = nil;
//
//	principalVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//	
//	[self presentModalViewController:principalVC animated:YES];
//	[UIView commitAnimations];
//	[principalVC release];
    
    if ([self.txtMesaEscolhida.text isEqualToString:@""] && [self.mesaEscolhida isEqualToString:@""]) {
        
        self.selectorParaExecutar = nil;
        [self alertaMesaNaoEscolhida];
        
    } else if ([self.mesaEscolhida isEqualToString:@""]) {
        
        self.mesaEscolhida = self.txtMesaEscolhida.text;
        
    }
    
    if (![self.mesaEscolhida isEqualToString:@""]) {
        
        NSLog(@"Abrindo mesa %@", self.mesaEscolhida);
        
        Garcon* operador = self.appDelegate.operador;
                
        UITabBarController* tabBarController = [[UITabBarController alloc] init];
        
        PedidoViewController* pedidoVC = [[PedidoViewController alloc] initWithNibName:@"PedidoViewController" bundle:nil];
        pedidoVC.pedido.operador = operador;
        pedidoVC.pedido.mesa = self.mesaEscolhida;

        ContaViewController* contaVC = [[ContaViewController alloc] initWithNibName:@"ContaViewController" bundle:nil];
        contaVC.conta.mesa = self.mesaEscolhida;
        contaVC.pedidoViewController = pedidoVC;
        
        MenuViewController* menuVC = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
        menuVC.pedidoViewController = pedidoVC;
        menuVC.contaViewController = contaVC;
        
        pedidoVC.menuVC = menuVC;
        
        CancelaItensViewController* cancelaItensVC = [[CancelaItensViewController alloc] initWithNibName:@"CancelaItensViewController" bundle:nil];
        cancelaItensVC.conta.mesa = self.mesaEscolhida;
        cancelaItensVC.pedidoViewController = pedidoVC;
        
        UINavigationController* contaNavCon = [[UINavigationController alloc] initWithRootViewController:contaVC];
        UINavigationController* menuNavCon = [[UINavigationController alloc] initWithRootViewController:menuVC];
        UINavigationController* pedidoNavCon = [[UINavigationController alloc] initWithRootViewController:pedidoVC];
        UINavigationController* cancelaItensNavCon = [[UINavigationController alloc] initWithRootViewController:cancelaItensVC];
        
        NSMutableArray* viewControllers = [NSMutableArray arrayWithObjects: menuNavCon, pedidoNavCon, contaNavCon, nil];
        
        NSLog(@"operador.pode_cancelar: %d", operador.pode_cancelar);

        if (operador.pode_cancelar == YES) {
            [viewControllers addObject:cancelaItensNavCon];
        }
        
        tabBarController.viewControllers = viewControllers;
        
        self.txtMesaEscolhida.text = @"";
        self.mesaEscolhida = @"";
        tabBarController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentModalViewController:tabBarController animated:YES];
    }
}


- (void) executaAbrirMapaMesas {
    NSLog(@"executaAbrirMapaMesas...");
	MesasMapaViewController* mapaVC = [[MesasMapaViewController alloc] init];
	
	mapaVC.escolherMesaVC = self;
    
    mapaVC.mesas = self.mesas;
    
    NSMutableDictionary* conf = self.appDelegate.configuracao;
    if ([[conf objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        mapaVC.navigationItem.title = @"Mapa das Comandas";
    } else {
        mapaVC.navigationItem.title = @"Mapa das Mesas";
    }
	
	[self.navigationController pushViewController:mapaVC animated:YES];
    
}

- (IBAction) abrirMapaMesas: (id) sender {
    self.viewCarregando.hidden = NO;
    [self carregaMapaMesas:@selector(executaAbrirMapaMesas)];
//    [self performSelector:@selector(executaAbrirMapaMesas) withObject:nil afterDelay:5];
}

- (NSArray*) calculaMesasAbertas {
    NSMutableArray* mesasAbertas = [[NSMutableArray alloc] init];
    for (Mesa* tauba in self.mesas) {
        if ([tauba.status isEqualToString:@"-1"]) {
            [mesasAbertas addObject:tauba];
        }
    }
    return mesasAbertas;
}

- (NSArray*) calculaMesasContaAprovada {
    NSMutableArray* mesasContaAprovada = [[NSMutableArray alloc] init];
    for (Mesa* tauba in self.mesas) {
        if ([tauba.status isEqualToString:@"1"]) {
            [mesasContaAprovada addObject:tauba];
        }
    }
    return mesasContaAprovada;
}

- (NSArray*) calculaMesasLimpas {
    NSMutableDictionary* mesasLimpasDic = [[NSMutableDictionary alloc] init];
    
    for (int i = 1; i < 1000 ; i++) {
        Mesa* taubaLimpa = [[Mesa alloc] init];
        taubaLimpa.mesa = [NSString stringWithFormat:@"%d", i];
        [mesasLimpasDic setObject:taubaLimpa forKey:[NSString stringWithFormat:@"%d", i]];
    }

    for (Mesa* tauba in self.mesas) {
        [mesasLimpasDic removeObjectForKey:tauba.mesa];
    }
        
    NSMutableArray* mesasLimpasMutArr = [NSMutableArray arrayWithArray:[mesasLimpasDic allValues]];
    
    [mesasLimpasMutArr sortUsingFunction:comparaMesas context:nil];
    
    return [NSArray arrayWithArray:mesasLimpasMutArr];;
}

- (void) executaAbrirStatusMesas {
    NSLog(@"executaAbrirStatusMesas...");
	UITabBarController* tabBarController = [[UITabBarController alloc] init];
    
    NSString* strTitulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        strTitulo = @"Comandas";
    } else {
        strTitulo = @"Mesas";
    }
    	
	MesasViewController* mesasAbertasVC = [[MesasViewController alloc] initWithTitle:[NSString stringWithFormat:@"%@ Abertas", strTitulo]];
    mesasAbertasVC.mesas = [self calculaMesasAbertas];
    mesasAbertasVC.escolherMesaVC = self;
	
    MesasViewController* mesasContaAprovadaVC = [[MesasViewController alloc] initWithTitle:@"Contas Aprovadas"];
    mesasContaAprovadaVC.mesas = [self calculaMesasContaAprovada];
    mesasContaAprovadaVC.escolherMesaVC = self;
	
    MesasViewController* mesasLimpasVC = [[MesasViewController alloc] initWithTitle:[NSString stringWithFormat:@"%@ Limpas", strTitulo]];
    mesasLimpasVC.mesas = [self calculaMesasLimpas];
    mesasLimpasVC.escolherMesaVC = self;
	
	NSArray* viewControllers = [NSArray arrayWithObjects: mesasAbertasVC, mesasContaAprovadaVC, mesasLimpasVC, nil];
	
	tabBarController.viewControllers = viewControllers;
	
	tabBarController.navigationItem.title = [NSString stringWithFormat:@"Status das %@", strTitulo];
    
	[self.navigationController pushViewController:tabBarController animated:YES];
}

- (IBAction) abrirStatusMesas: (id) sender {
    self.viewCarregando.hidden = NO;
    [self carregaMapaMesas:@selector(executaAbrirStatusMesas)];
//    [self performSelector:@selector(executaAbrirStatusMesas) withObject:nil afterDelay:5];
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization.
//    }
//    return self;
//}

- (void) viewWillAppear: (BOOL)animated {
	[super viewWillAppear:animated];
    
    [self.txtMesaEscolhida becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	[self.txtMesaEscolhida resignFirstResponder];
}

- (void) sairDaApp {
    exit(0);
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = [NSString stringWithString:@"Nova Mesa"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Sair" style:UIBarButtonItemStylePlain target:self action:@selector(sairDaApp)];

    NSMutableDictionary* conf = self.appDelegate.configuracao;
    if ([[conf objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        NSLog(@"Modo Comanda!");
        self.txtMesaEscolhida.placeholder = @"Digite a comanda";
        self.lblCarregandoMesas.text = @"Carregando Comandas";
        self.title = @"Nova Comanda";
    }
    
    Garcon* operador = self.appDelegate.operador;
	self.lblNomeDoGarcon.text = operador.nome;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.viewCarregando.hidden = YES;
    
    NSLog(@"Mesa escolhida: %@", self.mesaEscolhida);
    
	if (![self.mesaEscolhida isEqualToString:@""]) {
		[self abrirMesa:nil];
	}    
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setLblCarregandoMesas:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}


//Textfield methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 3);
}



@end
