//
//  PesquisaProdutoViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/31/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPedido.h"
#import "MenuViewController.h"

@interface PesquisaProdutoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UISearchDisplayDelegate, UISearchBarDelegate>

@property (nonatomic, strong) NSString* mesa;
@property (nonatomic, strong) ItemPedido* itemSelecionado;
@property (nonatomic, strong) NSFetchedResultsController* produtosResultsController;
@property (nonatomic, strong) NSMutableArray* produtosFiltrados;
@property (nonatomic, strong) IBOutlet UITableView* tbvProdutos;
@property (nonatomic, strong) MenuViewController* menuVC;
@property (nonatomic, strong) NSString* categoriaId;

@end
