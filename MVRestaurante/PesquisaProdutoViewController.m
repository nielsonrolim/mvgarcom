//
//  PesquisaProdutoViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/31/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import "PesquisaProdutoViewController.h"
#import "AppDelegate.h"
#import "Produto.h"

@implementation PesquisaProdutoViewController

@synthesize mesa = _mesa;
@synthesize itemSelecionado = _itemSelecionado;
@synthesize produtosResultsController = _produtosResultsController;
@synthesize produtosFiltrados = _produtosFiltrados;
@synthesize tbvProdutos = _tbvProdutos;
@synthesize menuVC = _menuVC;
@synthesize categoriaId = _categoriaId;


- (void) configuraItemPedido: (Produto*) produto {
	ItemPedido* novoItemPedido =[[ItemPedido alloc] init];
	novoItemPedido.produto = produto;
	novoItemPedido.quantidade = [NSNumber numberWithInt:1];
		
	self.itemSelecionado = novoItemPedido;
	
	
}

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.title = @"Produtos"; //[NSString stringWithFormat:@"%@", self.mesa];

    self.produtosFiltrados = [[NSMutableArray alloc] init];

	if (self.produtosResultsController != nil) {
		return;
	}	
	
	AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext* managedObjectContext = appDelegate.managedObjectContext;
	
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription* produtoEntity = [NSEntityDescription entityForName:@"Produto" inManagedObjectContext:managedObjectContext];
	[request setEntity:produtoEntity];
	
    [NSFetchedResultsController deleteCacheWithName:@"produtos_list.cache"]; 
    
    if ([self.categoriaId isEqualToString:@""] || self.categoriaId == nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promocao != 'S' AND categoria.oculto != 'S'"];
        [request setPredicate:predicate];
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"promocao != 'S' AND categoria.oculto != 'S' AND categoria.codigo == %@", self.categoriaId]];
        [request setPredicate:predicate];
    }
    
	
	NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nome" ascending:YES];
	NSArray* sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSFetchedResultsController* fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:@"produtos_list.cache"];
	fetchedResultsController.delegate = self;
	
	NSError *error;
	BOOL success = [fetchedResultsController performFetch:&error];
	if (!success) {
		//Handle the error
	}
	
	/*
	 NSError* error;
	 NSMutableArray* mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
	 
	 if (mutableFetchResults == nil) {
	 //Tratar o erro...
	 }
	 
	 self.items = mutableFetchResults;
	 [mutableFetchResults release];
	 
	 */
	
	self.produtosResultsController = fetchedResultsController;
	[self.tbvProdutos reloadData];
	
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}
*/

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.produtosResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.produtosFiltrados count];
    } else {
        return [[[self.produtosResultsController sections] objectAtIndex:section] numberOfObjects];
    }
	
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
	Produto* produto = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		produto = [self.produtosFiltrados objectAtIndex:indexPath.row];
	} else {
		produto = [self.produtosResultsController objectAtIndexPath:indexPath];
	}
	cell.textLabel.text = produto.nome;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
	
	Produto* produto = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		produto = [self.produtosFiltrados objectAtIndex:indexPath.row];
	} else {
		produto = [self.produtosResultsController objectAtIndexPath:indexPath];
	}
		
	self.itemSelecionado.produto = produto;
	self.itemSelecionado.quantidade = [NSNumber numberWithInt:1];
	
	[self.menuVC configuraItemPedido:self.menuVC.itemSelecionado.produto];
		
//	[self dismissModalViewControllerAnimated:YES];
	[self.navigationController popToRootViewControllerAnimated:YES];

}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
	
	[self.produtosFiltrados removeAllObjects];
	
	Produto* produto = nil;
	for (NSUInteger i = 0; i < [[self.produtosResultsController fetchedObjects] count]; i++) {
		produto = [self.produtosResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];

        // Busca em qualquer parte da string
//        if ([produto.nome rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)].location != NSNotFound)
//        {
//            //Yes it does contain string
//            [self.produtosFiltrados addObject:produto];
//        }
        
        // Busca somente no início da string
		NSComparisonResult result = [produto.nome compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
		if (result == NSOrderedSame)
		{
			[self.produtosFiltrados addObject:produto];
		}
	}
	
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	
	[self filterContentForSearchText:searchString scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
	
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	[self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
	
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}





@end

