//
//  LoginViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "CarregaMenuViewController.h"
#import "DropDownCell.h"
#import "SenhaTableViewCell.h"
#import "LoginBotaoTableViewCell.h"
#import "SMXMLDocument.h"
#import "Garcon.h"
#import "ConstantesConf.h"

@implementation LoginViewController

@synthesize dropDownOpen = _dropDownOpen;
@synthesize wsAtivo = _wsAtivo;
@synthesize garcons = _garcons;
@synthesize garconSelecionadoId = _garconSelecionadoId;
@synthesize txtSenha = _txtSenha;
@synthesize txtSetor = _txtSetor;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

NSInteger comparaGarcons(id arg1, id arg2, void *arg3) {
    return [[(Garcon*)arg1 nome] compare: [(Garcon*)arg2 nome]];
	//Swap arg1 and arg2 to reverse the sort order
}


// Alertas
- (void) alertaProblemaConexao {
	NSString* msg_alert = [NSString stringWithString:@"Não foi possível conectar ao servidor. Tentar novamente?"];
	
	UIAlertView *alert = [[UIAlertView alloc] init];
	alert.title = @"Problema de conexão";
	alert.message = msg_alert;
	alert.delegate = self;
	alert.tag = 1;
	[alert addButtonWithTitle:@"Tentar novamente"];
	[alert addButtonWithTitle:@"Cancelar"];
    [alert addButtonWithTitle:@"Sair da aplicação"];
	[alert show];
}

- (void) alertaSenhaIncorreta {
    NSString* titulo_alert = @"Senha inconrreta.";
    NSString* msg_alert = @"A senha digitada não está correta. Tente novamente.";
	
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo_alert
                                                    message:msg_alert
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    alert.tag = 2;
    [alert show];
}

- (void) alertaGarconNaoSelecionado {
    NSString* titulo_alert = @"Garçon não selecionado.";
    NSString* msg_alert = @"Selecione o garçon para acessar.";
	
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo_alert
                                                    message:msg_alert
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    alert.tag = 3;
    [alert show];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (alertView.tag == 1) {
		if (buttonIndex == 0) {
			[self executaLoginSeWSAtivo];
		} else if (buttonIndex == 1) {
            NSLog(@"Cancelando");
		} else if (buttonIndex == 2) {
			exit(0);
		}		
	} else if (alertView.tag == 2) {
        
    }
}

// Métodos do login

- (void) executaLoginSeWSAtivo {
	[[RKClient sharedClient] get:@"/ativo" delegate:self];
}

- (void) executaLogar {
	if (self.wsAtivo) {
        
            self.appDelegate.operador = [self.garcons objectAtIndex:self.garconSelecionadoId];
            self.appDelegate.operador.setor = self.txtSetor.text;
			
			CarregaMenuViewController* carregaMenuVC = [[CarregaMenuViewController alloc] initWithNibName:@"CarregaMenuViewController" bundle:nil];
			carregaMenuVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
			[self presentModalViewController:carregaMenuVC animated:YES];
	}
}	


- (void) carregaGarcon {
	
	RKClient* rkClient = [RKClient sharedClient];
	
	if ([rkClient isNetworkAvailable]) {
		[rkClient get:@"/garcon" delegate:self];
	} else {
		[self alertaProblemaConexao];
	}
	
}

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
    if ([response isNotFound]) {
        [self alertaProblemaConexao];
    }
    
	if ([request isGET]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
			
			if ([[document.root value] isEqualToString:@"ATIVO"]) {
				self.wsAtivo = YES;
                [self executaLogar];
			} else {
				NSMutableArray* garconsTmp = [[NSMutableArray alloc] init];
                
                Garcon* ninguem = [[Garcon alloc] init];
                ninguem.numero = @"0";
                ninguem.nome = @" Selecione o Garçon ";
                ninguem.senha = @"989049#$ˆ$#%ˆGsdfg#$%ˆ#$%gdfgd";
                
                [garconsTmp addObject:ninguem];
				
				for (SMXMLElement* garconXml in [document.root childrenNamed:@"garcon"] ) {
                    NSLog(@"Nome: %@", [garconXml valueWithPath:@"nome"]);
					NSLog(@"Senha: %@", [garconXml valueWithPath:@"senha"]);
					Garcon* juberlito = [[Garcon alloc] init];
					juberlito.numero = [garconXml valueWithPath:@"numero"];
					juberlito.nome = [garconXml valueWithPath:@"nome"];
					juberlito.senha = [garconXml valueWithPath:@"senha"];
                    juberlito.pode_cancelar = ([[garconXml valueWithPath:@"pode_cancelar"] isEqualToString:@"S"]) ? YES : NO;
					[garconsTmp addObject:juberlito];
				}
				
                [garconsTmp sortUsingFunction:comparaGarcons context:nil];
				
				self.garcons = [NSArray arrayWithArray:garconsTmp];
			}				
            
		}
	}
	
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	NSLog(@"INATIVO... Falhou");
	self.wsAtivo = NO;
	[self alertaProblemaConexao];
}

//Ciclo de vida da view

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization.
		self.title = @"Acesso Restrito";
        
        self.wsAtivo = NO;
        
        [self carregaGarcon];
	}
	return self;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) sairDaApp {
    exit(0);
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Sair" style:UIBarButtonItemStylePlain target:self action:@selector(sairDaApp)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            if (self.dropDownOpen) {
                return [self.garcons count];
            } else {
                return 1;
            }
            break;
            
        default:
            return 1;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    static NSString* DropDownCellIdentifier = @"DropDownCell";
    static NSString* SenhaCellIdentifier = @"SenhaTableViewCell";
    static NSString* AcessarCellIdentifier = @"AcessarCell";
    
    switch ([indexPath section]) {
        case 0: {
            
            switch ([indexPath row]) {
                case 0: {
                    DropDownCell *cell = (DropDownCell*) [tableView dequeueReusableCellWithIdentifier:DropDownCellIdentifier];
                    
                    if (cell == nil){
                        
                        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DropDownCell" owner:nil options:nil];
                        
                        for(id currentObject in topLevelObjects)
                        {
                            if([currentObject isKindOfClass:[DropDownCell class]])
                            {
                                cell = (DropDownCell *)currentObject;
                                break;
                            }
                        }
                    }
                    
                    if (self.garcons == nil) {
                        cell.textLabel.text = @" Selecione o Garçon ";
                    } else {
                        cell.textLabel.text = [[self.garcons objectAtIndex:self.garconSelecionadoId] nome];
                    }
                    
                    // Configure the cell.
                    return cell;
                    
                    break;
                }
                default: {
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    if (cell == nil) {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    }
                    
                    NSString *label = [NSString stringWithFormat:@"%@", [[self.garcons objectAtIndex:[indexPath row]] nome]];
                    
                    cell.textLabel.text = label;
                    
                    // Configure the cell.
                    cell.textLabel.textAlignment = UITextAlignmentLeft;
                    cell.textLabel.textColor = [UIColor blackColor];
                    cell.backgroundColor = [UIColor whiteColor];

                    return cell;
                    
                    break;
                }
            }
            
            break;
        }
        case 1: {
            
            SenhaTableViewCell* cell = (SenhaTableViewCell*) [tableView dequeueReusableCellWithIdentifier:SenhaCellIdentifier];
            
            if (cell == nil){
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SenhaTableViewCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[SenhaTableViewCell class]])
                    {
                        cell = (SenhaTableViewCell*)currentObject;
                        break;
                    }
                }
            }
            
            // Configure the cell.
            
            self.txtSenha = cell.txtSenha;
            return cell;
            
            break;
        }
        case 2: {
            
            LoginBotaoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AcessarCellIdentifier];
            
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LoginBotaoTableViewCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[LoginBotaoTableViewCell class]])
                    {
                        cell = (LoginBotaoTableViewCell*)currentObject;
                        break;
                    }
                }
            }
            
            
            // Configure the cell.
//            cell.textLabel.text = @"Acessar";
//            cell.textLabel.textAlignment = UITextAlignmentCenter;
//            cell.textLabel.textColor = [UIColor colorWithRed:53.0 / 255 green:115.0 / 255 blue:19.0 / 255 alpha:1.0];
            
            self.txtSetor = cell.txtSetor;
            
            NSMutableDictionary* conf = self.appDelegate.configuracao;
            if (conf == nil) {
                conf = [[NSMutableDictionary alloc] init];
            }
            
            NSLog(@"Garçom por setor::::::: %@", [conf objectForKey:MESASPORSETOR_KEY]);
            
            if ([[conf objectForKey:MESASPORSETOR_KEY] isEqualToString:@"NO"]) {
                cell.lblSetor.hidden = YES;
                cell.txtSetor.hidden = YES;                
            }
            return cell;
            
            break;
        }
        default:
            
            return nil;
            break;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    switch ([indexPath section]) {
        case 0: {
            switch ([indexPath row]) {
                case 0:
                {
                    [self.txtSenha resignFirstResponder];
                    
                    DropDownCell *cell = (DropDownCell*) [tableView cellForRowAtIndexPath:indexPath];
                    
                    NSMutableArray* indexPathArray = [[NSMutableArray alloc] initWithCapacity:[self.garcons count]];
                    
                    for (int i = 1; i < [self.garcons count]; i++) {
                        NSIndexPath* path0 = [NSIndexPath indexPathForRow:[indexPath row]+i inSection:[indexPath section]];
                        [indexPathArray addObject:path0];
                    }
                                        
                    if ([cell isOpen])
                    {
                        [cell setClosed];
                        self.dropDownOpen = [cell isOpen];
                        
                        [tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
                    }
                    else
                    {
                        [cell setOpen];
                        self.dropDownOpen = [cell isOpen];
                        
                        [self.txtSenha resignFirstResponder];
                        
                        [tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
                    }
                    
                    break;
                }   
                default:
                {
                    self.garconSelecionadoId = [indexPath row];
                    
                    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:[indexPath section]];
                    DropDownCell *cell = (DropDownCell*) [tableView cellForRowAtIndexPath:path];
                    
                    cell.textLabel.text = [[self.garcons objectAtIndex:self.garconSelecionadoId] nome];
                    
                    // close the dropdown cell
                    
                    NSMutableArray* indexPathArray = [[NSMutableArray alloc] initWithCapacity:[self.garcons count]];
                    
                    for (int i = 1; i < [self.garcons count]; i++) {
                        NSIndexPath *path0 = [NSIndexPath indexPathForRow:[path row]+i inSection:[indexPath section]];
                        [indexPathArray addObject:path0];
                    }
                                        
                    [cell setClosed];
                    self.dropDownOpen = [cell isOpen];
                    
                    [tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
                    
                    NSMutableDictionary* conf = self.appDelegate.configuracao;
                    
                    if (([[conf objectForKey:MODODEMONSTRACAO_KEY] isEqualToString:@"YES"] || [conf count] == 0) && ([[[self.garcons objectAtIndex:self.garconSelecionadoId] nome] isEqualToString:@"MVARANDAS"])) {
                        self.txtSenha.text = DEMOSENHA;
                    } else {
                        [self.txtSenha becomeFirstResponder];
                    }
                    
                    break;
                }
            }
            
            break;
        }
        case 2: {
            
            if (self.garconSelecionadoId == 0) {
                [self alertaGarconNaoSelecionado];
            } else {                
                if (![self.txtSenha.text isEqualToString:[[self.garcons objectAtIndex:self.garconSelecionadoId] senha]]) {
                    [self alertaSenhaIncorreta];
                } else {
                    [self.txtSenha resignFirstResponder];
                    if ([[RKClient sharedClient] isNetworkAvailable]) {
                        [self executaLoginSeWSAtivo];
                    } else {
                        [self alertaProblemaConexao];
                    }
                }
            }
            
            break;
        }
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
