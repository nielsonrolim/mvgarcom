//
//  CustomAlert.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlert : UIAlertView

+ (void) setBackgroundColor:(UIColor *) background 
            withStrokeColor:(UIColor *) stroke;

@end
