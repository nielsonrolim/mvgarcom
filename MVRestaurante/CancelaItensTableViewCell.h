//
//  CancelaItensTableViewCell.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ItemConta;

@interface CancelaItensTableViewCell : UITableViewCell

@property (nonatomic, strong) ItemConta* item;
@property (nonatomic, strong) IBOutlet UILabel* nome;
@property (nonatomic, strong) IBOutlet UILabel* preco;
@property (nonatomic, strong) IBOutlet UILabel* subtotal;
@property (nonatomic, strong) IBOutlet UILabel* lblCompl;
@property (nonatomic, strong) IBOutlet UILabel* lblObs;
@property (nonatomic, strong) IBOutlet UILabel* lblQuantidade;

- (IBAction) adicionaQuantidade: (id) sender;
- (IBAction) subtraiQuantidade: (id) sender;
- (void) reloadData;

@end
