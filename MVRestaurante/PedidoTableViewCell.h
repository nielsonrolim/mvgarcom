//
//  PedidoTableViewCell.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/10/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPedido.h"
#import "PedidoViewController.h"

@interface PedidoTableViewCell : UITableViewCell {
	IBOutlet UILabel* nome;
	IBOutlet UILabel* quantidade;
	IBOutlet UILabel* subtotal;
	IBOutlet UIButton* btnAdicionaQuantidade;
	IBOutlet UIButton* btnSubtraiQuantidade;
	PedidoViewController* pedidoVC;
	ItemPedido* item;
	
	IBOutlet UILabel* lblCompl;
	IBOutlet UILabel* lblObs;
}

@property (nonatomic, strong) IBOutlet UILabel* nome;
@property (nonatomic, strong) IBOutlet UILabel* quantidade;
@property (nonatomic, strong) IBOutlet UILabel* subtotal;
@property (nonatomic, strong) IBOutlet UIButton* btnAdicionaQuantidade;
@property (nonatomic, strong) IBOutlet UIButton* btnSubtraiQuantidade;
@property (nonatomic, strong) PedidoViewController* pedidoVC;
@property (nonatomic, strong) ItemPedido* item;

@property (nonatomic, strong) IBOutlet UILabel* lblCompl;
@property (nonatomic, strong) IBOutlet UILabel* lblObs;

- (IBAction) adicionaQuantidade: (id) sender;
- (IBAction) subtraiQuantidade: (id) sender;
- (void) reloadData;

@end
