//
//  CarregaMenuViewController.h
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 10/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface CarregaMenuViewController : UIViewController <RKObjectLoaderDelegate> {
	NSManagedObjectContext* managedObjectContext;
}

@property (nonatomic, assign) BOOL novoUsuario;
@property (nonatomic, strong) NSURLRequest* reqLogoRestaurante;
@property (nonatomic, strong) UIImage* imgLogoRestaurante;

@end
