//
//  Conta.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Conta : NSObject

@property (nonatomic, strong) NSMutableArray* items;
@property (nonatomic, strong) NSString* mesa;

- (BOOL) estaVazio;
- (NSDecimalNumber*) subtotal;
- (NSDecimalNumber*) taxaDeServico;
- (NSDecimalNumber*) total;

@end
