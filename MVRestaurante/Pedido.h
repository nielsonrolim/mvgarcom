//
//  Pedido.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 11/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Garcon.h"

@interface Pedido : NSObject

@property (nonatomic, strong) NSMutableArray* items;
@property (nonatomic, strong) Garcon* operador;
@property (nonatomic, strong) NSString* mesa;

- (NSString*) escreveLinhaPedido:(NSString*)mesaModoComanda;
- (BOOL) estaVazio;

@end
