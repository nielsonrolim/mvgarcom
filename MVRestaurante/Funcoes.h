//
//  Funcoes.h
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 11/16/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Funcoes : NSObject

NSString* formataNumeroComVirgula (NSDecimalNumber* numeroDecimal);
BOOL verificaSessaoExpirada(NSDate* sessaoInicioTimestamp);

@end
