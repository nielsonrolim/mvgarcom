//
//  CarregaMenuViewController.m
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 10/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CarregaMenuViewController.h"
#import "AppDelegate.h"
#import "EscolherMesaViewController.h"
#import "Tbprod.h"
#import "Tbcateg.h"
#import "Tbobs.h"
#import "Produto.h"
#import "Categoria.h"
#import "Obs.h"

@implementation CarregaMenuViewController

@synthesize novoUsuario = _novoUsuario;
@synthesize reqLogoRestaurante = _reqLogoRestaurante;
@synthesize imgLogoRestaurante = _imgLogoRestaurante;

- (NSManagedObjectContext*) managedObjectContext {
	if (!managedObjectContext) {
		AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
		managedObjectContext = appDelegate.managedObjectContext;
	}
	return managedObjectContext;
}

- (void)loadProdutos {
    // Load the object model via RestKit	
    RKObjectManager* objectManager = [RKObjectManager sharedManager];
    [objectManager loadObjectsAtResourcePath:@"/produtos" delegate:self block:^(RKObjectLoader* loader) {

        if ([objectManager.acceptMIMEType isEqualToString:RKMIMETypeJSON]) {
            loader.objectMapping = [objectManager.mappingProvider objectMappingForClass:[Tbprod class]];
        }
    }];
}

- (void)loadCategorias {
	// Load the object model via RestKit	
	RKObjectManager* objectManager = [RKObjectManager sharedManager];
    
    [objectManager loadObjectsAtResourcePath:@"/categ" delegate:self block:^(RKObjectLoader* loader) {
        if ([objectManager.acceptMIMEType isEqualToString:RKMIMETypeJSON]) {
            loader.objectMapping = [objectManager.mappingProvider objectMappingForClass:[Tbcateg class]];
        }
    }];	
}

- (void) loadObservacoes {
	// Load the object model via RestKit	
	RKObjectManager* objectManager = [RKObjectManager sharedManager];
	
    [objectManager loadObjectsAtResourcePath:@"/observ" delegate:self block:^(RKObjectLoader* loader) {
        if ([objectManager.acceptMIMEType isEqualToString:RKMIMETypeJSON]) {
            loader.objectMapping = [objectManager.mappingProvider objectMappingForClass:[Tbobs class]];
        }
    }];
}


- (void) iniciaApp {
    
    EscolherMesaViewController* escolherMesaVC = [[EscolherMesaViewController alloc] initWithNibName:@"EscolherMesaViewController" bundle:nil];
	UINavigationController *escolherMesaNavCon = [[UINavigationController alloc] initWithRootViewController:escolherMesaVC];
	escolherMesaNavCon.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:escolherMesaNavCon animated:YES];

}

- (id) init {
    self = [super initWithNibName:@"CarregaMenuViewController" bundle:nil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [self init];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) loadImageInBackground {
    NSURL* urlLogoRestaurante = [NSURL URLWithString: @"http://images.colourbox.com/thumb_COLOURBOX1948115.jpg"];
    self.imgLogoRestaurante = [UIImage imageWithData: [NSData dataWithContentsOfURL:urlLogoRestaurante]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"CarregaMenuViewController viewDidLoad...");
    
    [NSThread detachNewThreadSelector:@selector(loadImageInBackground)
                             toTarget:self withObject:nil];
    
    NSString* url = [NSString stringWithFormat:@"http://images.colourbox.com/thumb_COLOURBOX1948115.jpg"];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.reqLogoRestaurante = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Ciclo de Vida
    //  - loadCategorias:
    //  - objectLoader:didLoadObjects:
    //      - gravaCategorias:
    //      - loadProdutos:
    //  - objectLoader:didLoadObjects:
    //      - gravaProdutos:
    //      - loadObservacoes:
    //  - objectLoader:didLoadObjects:
    //      - gravaObservacoes:
    //      - carregaEscolherMesaVC
    
    [self loadCategorias];

//    [self performSelector:@selector(iniciaApp) withObject:nil afterDelay:5];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.novoUsuario == YES) {
        self.novoUsuario = NO;
        [self dismissModalViewControllerAnimated:NO];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark CoreData Methods

- (void)saveContext {
	NSError *error = nil;
	
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

- (void) deleteAllObjects: (NSString *) entityDescription  {
	
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
	
    NSError* error;
    NSArray* items = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
	
	
    for (NSManagedObject* managedObject in items) {
        [managedObjectContext deleteObject:managedObject];
    }
	
    [self saveContext];
	
}


- (void)gravaCategoriasNoBD:(NSArray*) objetos {
    
	NSEntityDescription* categoriaEntidy = [NSEntityDescription
                                            entityForName:@"Categoria"
                                            inManagedObjectContext:self.managedObjectContext];
	
	[self deleteAllObjects:@"Categoria"];
	
	NSMutableDictionary* categorias_com_complemento = [[NSMutableDictionary alloc] init];
	
	for (int i = 0; i < [objetos count]; i++) {
		
		Tbcateg* categoria = [objetos objectAtIndex:i];
        
		Categoria *novaCategoria = (Categoria *)[[NSManagedObject alloc]
                                    initWithEntity:categoriaEntidy
                                    insertIntoManagedObjectContext:managedObjectContext];
		
		novaCategoria.categoriaId = categoria.identifier;
		novaCategoria.fracao = categoria.fracao;
		novaCategoria.metodo = categoria.metodo;
		novaCategoria.c_complemento = categoria.c_complemento;
		novaCategoria.nome = categoria.nome;
		novaCategoria.codigo = categoria.codigo;
		novaCategoria.tipo = categoria.tipo;
		novaCategoria.pede_observ = categoria.pede_observ;
		novaCategoria.pede_complemento = categoria.pede_complemento;
		novaCategoria.e_complemento = categoria.e_complemento;
		novaCategoria.peso = categoria.peso;
        novaCategoria.oculto = categoria.oculto;
        
        if ([novaCategoria.c_complemento intValue] > 0) {
			[categorias_com_complemento setObject:novaCategoria.c_complemento forKey:novaCategoria.categoriaId];            
		}
	}
	
	NSArray* categoriaIds = [categorias_com_complemento allKeys];
	
	for (NSString* categoriaId in categoriaIds) {
		NSFetchRequest* request = [[NSFetchRequest alloc] init];
		
		[request setEntity:categoriaEntidy];
		
		NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"codigo == %@", categoriaId]];
		[request setPredicate:predicate];
		
		NSError* error;
        NSArray* fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
		
		Categoria* categoria = nil;
		
		if ([fetchResults count] != 0) {
			categoria = [fetchResults objectAtIndex:0];
		}		
        
		predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"codigo == %@", [categorias_com_complemento objectForKey:categoriaId]]];
		[request setPredicate:predicate];
		
        fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
		
		if ([fetchResults count] != 0) {
			Categoria* categoriaComplemento = [fetchResults objectAtIndex:0];
			categoria.categoria_complemento = categoriaComplemento;
		}		
		
	}
	
	[self saveContext];
}

- (void)gravaObservacoesNoBD:(NSArray*) objetos {
	
	NSEntityDescription* obsEntidy = [NSEntityDescription
                                      entityForName:@"Obs"
                                      inManagedObjectContext:self.managedObjectContext];
	
	[self deleteAllObjects:@"Obs"];
    
	for (int i = 0; i < [objetos count]; i++) {
		
		Tbobs* obs = [objetos objectAtIndex:i];
		
		Obs *novaObs = (Obs *)[[NSManagedObject alloc]
                        initWithEntity:obsEntidy
                        insertIntoManagedObjectContext:self.managedObjectContext];
		novaObs.cod = obs.codigo;
		novaObs.categ = obs.categ;
		novaObs.descri = obs.descri;
		novaObs.valor = obs.valor;
		
		NSFetchRequest* request = [[NSFetchRequest alloc] init];
		
		NSEntityDescription* categoriaEntidy = [NSEntityDescription
												entityForName:@"Categoria"
												inManagedObjectContext:self.managedObjectContext];
		
		[request setEntity:categoriaEntidy];
		
		NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"codigo == %@", novaObs.categ]];
		[request setPredicate:predicate];
		
		NSError* error;
		NSArray* fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
		
		if ([fetchResults count] != 0) {
			Categoria* categoria = [fetchResults objectAtIndex:0];
			
			novaObs.categoria = categoria;
		}
	}
	
	[self saveContext];
}

- (void)gravaProdutosNoBD:(NSArray*) objetos {
	
	NSEntityDescription* produtoEntidy = [NSEntityDescription
										  entityForName:@"Produto"
										  inManagedObjectContext:self.managedObjectContext];
	
	[self deleteAllObjects:@"Produto"];
	
	for (int i = 0; i < [objetos count]; i++) {
		
		Tbprod* produto = [objetos objectAtIndex:i];
		
		Produto *novoProduto = (Produto *)[[NSManagedObject alloc]
								initWithEntity:produtoEntidy
								insertIntoManagedObjectContext:self.managedObjectContext];
		
		novoProduto.produtoId = produto.identifier;
		novoProduto.codigo = produto.codigo;
		novoProduto.nome = produto.nome;
		novoProduto.preco = produto.preco;
		novoProduto.preco2 = produto.preco2;
		novoProduto.preco3 = produto.preco3;
		novoProduto.preco4 = produto.preco4;
		novoProduto.categ = produto.categ;
		novoProduto.tipo = produto.tipo;
		novoProduto.comanda = produto. comanda;
		novoProduto.taxa = produto.taxa;
		novoProduto.complemento = produto.complemento;
		novoProduto.observ = produto.observ;
		novoProduto.metodo = produto.metodo;
		novoProduto.tam = produto.tam;
		novoProduto.status = produto.status;
        novoProduto.promocao = produto.promocao;
		
		
        
		NSFetchRequest* request = [[NSFetchRequest alloc] init];
		
		NSEntityDescription* categoriaEntidy = [NSEntityDescription
												entityForName:@"Categoria"
												inManagedObjectContext:self.managedObjectContext];
		
		[request setEntity:categoriaEntidy];
		
		NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"codigo == %@", novoProduto.categ]];
		[request setPredicate:predicate];
		
		NSError* error;
		NSArray* fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
		
		if ([fetchResults count] != 0) {
			Categoria* categoria = [fetchResults objectAtIndex:0];
			
			novoProduto.categoria = categoria;
			
            //			NSLog(@"pede observ? %@", novoProduto.categoria.pede_observ);
            //			NSLog(@"Produto 1: %@", [[[categoria.produtos allObjects] objectAtIndex:0] nome]);
			
		}
		
        
	}
	
	[self saveContext];
}


#pragma mark RKObjectLoaderDelegate methods

//- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
//    NSLog(@"Loaded payload: %@", [response bodyAsString]);
//}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
//	NSLog(@"Loaded objects: %@", objects);    

	if ([[objects objectAtIndex:0] class] == [Tbprod class]) {
        [self gravaProdutosNoBD:objects];
        [self loadObservacoes];
	} else if (([[objects objectAtIndex:0] class] == [Tbcateg class])) {
        [self gravaCategoriasNoBD:objects];
        [self loadProdutos];
	} else if (([[objects objectAtIndex:0] class] == [Tbobs class])) {
        [self gravaObservacoesNoBD:objects];
        [self performSelector:@selector(iniciaApp) withObject:nil afterDelay:1];
	} else {
		NSLog(@"Ocorreu um erro... Classe não reconhecida.");
	}

}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    NSString* mensagem_erro = @"";
    if ([error code] == -1001) {
        mensagem_erro = @"Tempo excedido";
    } else {
        mensagem_erro = [error localizedDescription];
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Problema de conexão" message:mensagem_erro delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	NSLog(@"Hit error: %@", error);
    [self dismissModalViewControllerAnimated:YES];
}


@end
