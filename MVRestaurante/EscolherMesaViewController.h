//
//  EscolherMesaViewController.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 3/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@class AppDelegate;

@interface EscolherMesaViewController : UIViewController <RKRequestDelegate, UITextFieldDelegate>

@property (strong, nonatomic) AppDelegate* appDelegate;
@property (nonatomic, strong) IBOutlet UITextField* txtMesaEscolhida;
@property (nonatomic, strong) IBOutlet UIView* viewCarregando;
@property (nonatomic, strong) IBOutlet UILabel* lblNomeDoGarcon;
@property (strong, nonatomic) IBOutlet UILabel *lblCarregandoMesas;
@property (nonatomic, strong) NSString* mesaEscolhida;
@property (nonatomic, strong) NSArray* mesas;
@property (nonatomic, assign) BOOL wsAtivo;
@property (nonatomic, assign) SEL selectorParaExecutar; //POG Muito doido!!! Para guardar qual método foi executado para chamar o WS.

- (IBAction) abrirMesa: (id) sender;
- (IBAction) abrirMapaMesas: (id) sender;
- (IBAction) abrirStatusMesas: (id) sender;
- (void) carregaMapaMesas:(SEL)aSelector;
- (void) executaAbrirStatusMesas;
- (void) executaAbrirMapaMesas;

NSInteger comparaMesas(id arg1, id arg2, void *arg3);

@end
