//
//  CancelaItensPorGarcomViewController.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 17/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "Cancelamento.h"

@interface CancelaItensPorGarcomViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, RKRequestDelegate>

@property (nonatomic, strong) Cancelamento* cancelamento;
@property (nonatomic, assign) NSUInteger celulaSelecionada;
@property (strong, nonatomic) IBOutlet UITableView* tbvCancelaPorGarcom;
@property (nonatomic, strong) IBOutlet UILabel* lblMesa;
@property (nonatomic, strong) IBOutlet UILabel* lblSubtotal;
@property (nonatomic, strong) IBOutlet UILabel* lblTaxaServico;
@property (nonatomic, strong) IBOutlet UILabel* lblTotal;
@property (nonatomic, assign) BOOL wsAtivo;
@property (nonatomic, strong) IBOutlet UIView* viewCarregando;
@property (nonatomic, strong) IBOutlet UILabel* lblMsgAguarde;




@end
