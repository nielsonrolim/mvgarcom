//
//  Garcon.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Garcon.h"


@implementation Garcon

@synthesize numero = _numero;
@synthesize nome = _nome;
@synthesize senha = _senha;
@synthesize setor = _setor;
@synthesize pode_cancelar = _pode_cancelar;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


@end
