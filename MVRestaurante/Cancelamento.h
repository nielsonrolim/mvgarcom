//
//  Cancelamento.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemConta.h"

@interface Cancelamento : NSObject

@property (nonatomic, strong) ItemConta* itemConta;
@property (nonatomic, strong) NSMutableArray* items;

@end
