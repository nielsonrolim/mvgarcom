//
//  InicioViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface InicioViewController : UIViewController <FlipsideViewControllerDelegate, RKRequestDelegate>

@property (nonatomic, strong) IBOutlet UIButton* btnIniciar;
@property (nonatomic, strong) IBOutlet UIView* viewCarregando;
@property (nonatomic, assign) BOOL wsAtivo;
@property (strong, nonatomic) IBOutlet UILabel *lblVersao;

- (IBAction) iniciarAplicacao:(id)sender;
- (IBAction) abreConfiguracao:(id)sender;
- (IBAction) showInfo:(id)sender;
- (void) iniciaApp;

@end
