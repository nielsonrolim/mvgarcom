//
//  OutrasObservacoesViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPedido.h"

@interface OutrasObservacoesViewController : UIViewController <UITextViewDelegate>

@property (nonatomic, strong) ItemPedido* itemSelecionado;
@property (nonatomic, strong) IBOutlet UITextView* txvObservacoes;

- (IBAction) textFieldDoneEditing: (id) sender;
- (IBAction) salvar:(id)sender;
- (IBAction) cancelar:(id)sender;

@end
