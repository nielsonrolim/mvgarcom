//
//  Garcon.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Garcon : NSObject

@property (nonatomic, strong) NSString* numero;
@property (nonatomic, strong) NSString* nome;
@property (nonatomic, strong) NSString* senha;
@property (nonatomic, strong) NSString* setor;
@property (nonatomic, assign) BOOL pode_cancelar;


@end
