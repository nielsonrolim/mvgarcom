//
//  Mesa.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mesa : NSObject

@property (nonatomic, strong) NSString* mesa;
@property (nonatomic, strong) NSString* status;

@end
