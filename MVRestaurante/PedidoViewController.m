//
//  PedidoViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "PedidoViewController.h"
#import "ItemPedido.h"
#import "Produto.h"
#import "Obs.h"
#import "Garcon.h"
#import "PedidoTableViewCell.h"
#import "SMXMLDocument.h"
#import "MenuViewController.h"
#import "CustomAlert.h"
#import "ConstantesConf.h"

@interface PedidoViewController ()
@property (nonatomic, strong) UITextField* txtMesaModoComanda;
@end

@implementation PedidoViewController

@synthesize lblMesa = _lblMesa;
@synthesize lblSubTotal = _lblSubTotal;
@synthesize tbvItemsPedido = _tbvItemsPedido;
@synthesize btnRemoverItem = _btnRemoverItem;
@synthesize btnEnviaPedido = _btnEnviaPedido;
@synthesize viewCarregando = _viewCarregando;
@synthesize wsAtivo = _wsAtivo;
@synthesize menuVC = _menuVC;
@synthesize pedido = _pedido;
@synthesize lblMsgAguarde1 = _lblMsgAguarde1;
@synthesize txtMesaModoComanda = _txtMesaModoComanda;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

#pragma mark -
#pragma mark Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization.
		self.title = @"Pedido";
		
		UIImage* anImage = [UIImage imageNamed:@"shoppingcart.png"];
		UITabBarItem* theItem = [[UITabBarItem alloc] initWithTitle:@"Pedido" image:anImage tag:0];
		self.tabBarItem = theItem;
        
        self.pedido = [[Pedido alloc] init];
	}
	return self;
}


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"Comanda";
    } else {
        titulo = @"Mesa";
    }

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.leftBarButtonItem = self.editButtonItem;
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Nova %@", titulo] style:UIBarButtonItemStylePlain target:self action:@selector(novaMesa:)];
	self.lblMesa.text = [NSString stringWithFormat:@"%@ %@", titulo, self.pedido.mesa];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[self.tbvItemsPedido reloadData];
	self.lblSubTotal.text = [[NSString stringWithFormat:@"R$ %.2f", [[self calculaSubtotal] doubleValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
	self.wsAtivo = NO;
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 //	[self.tableView reloadData];
 //	[tbvItemsPedido reloadData];
 //	lblSubTotal.text = [NSString stringWithFormat:@"R$ %@", [[self calculaSubtotal] stringValue]];
 }
 */

/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.pedido.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	ItemPedido* itemPedido = (ItemPedido*)[self.pedido.items objectAtIndex:indexPath.row];
	
	if ( ([itemPedido.complementosSelecionados count] > 0) || ([itemPedido.observacoesSelecionadas count] > 0 || ![itemPedido.outrasObservacoes isEqualToString:@""]) ) {
		CGFloat altura = 105;
		int linhasAdicionais = [itemPedido.complementosSelecionados count] / 5;
		for (int i=0; i < linhasAdicionais; i++) {
			altura += 70;
		}
		return altura;
	} else {
		return 55;
	}
	
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	ItemPedido* itemPedido = (ItemPedido*)[self.pedido.items objectAtIndex:indexPath.row];
	BOOL temComplementos = NO;
	BOOL temObservacoes = NO;
	
	if ([itemPedido.observacoesSelecionadas count] > 0 || ![itemPedido.outrasObservacoes isEqualToString:@""]) {
		temObservacoes = YES;
	}
	
	if ([itemPedido.complementosSelecionados count] > 0) {
		temComplementos = YES;
	}
	
    static NSString* CellIdentifier = @"pedidoCell";
	
	PedidoTableViewCell* cell = (PedidoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	//	if (cell == nil){
	//	}
	
	NSArray* topLevelObjects = [NSArray array];
	
	if (temObservacoes || temComplementos) {
		if (temObservacoes && temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PedidoComplObsTableViewCell" owner:nil options:nil];
		} else if (temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PedidoComplTableViewCell" owner:nil options:nil];
		} else if (temObservacoes) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PedidoObsTableViewCell" owner:nil options:nil];
		}
		
	} else {
		topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PedidoTableViewCell" owner:nil options:nil];
	}
	
	for(id currentObject in topLevelObjects) {			
		if([currentObject isKindOfClass:[PedidoTableViewCell class]]) {
			cell = (PedidoTableViewCell *)currentObject;
			break;
		}
	}
	
	cell.pedidoVC = self;
	cell.item = itemPedido;
	cell.quantidade.hidden = NO;
	cell.btnAdicionaQuantidade.hidden = NO;
	cell.btnSubtraiQuantidade.hidden = NO;
	[cell reloadData];
	
	return cell;
	
	//    static NSString *CellIdentifier = @"Cell";
	//    
	//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	//    if (cell == nil) {
	//        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	//    }
	//    
	//    // Configure the cell...
	//    cell.textLabel.text = [(Produto*)[(ItemPedido*) [self.pedido.items objectAtIndex:indexPath.row] produto] nome];
	//	cell.detailTextLabel.text = [NSString stringWithFormat:@"Quant.: %@   Subtotal: %@", [[[self.pedido.items objectAtIndex:indexPath.row] quantidade] stringValue], [self calculaSubtotalPorItem:[self.pedido.items objectAtIndex:indexPath.row]] ];
	//    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
		[self.pedido.items removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		//		lblSubTotal.text = [NSString stringWithFormat:@"R$ %@", [[self calculaSubtotal] stringValue]];
		self.lblSubTotal.text = [[NSString stringWithFormat:@"R$ %.2f", [[self calculaSubtotal] doubleValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
		if ([self.pedido estaVazio]) {
			[self.btnRemoverItem setTitle:@"Remover" forState:UIControlStateNormal];
			[self.tbvItemsPedido setEditing:NO animated:YES];
		}
		//self.tbiPedido.badgeValue = [NSString stringWithFormat:@"%d", [self.pedido.items count]];
		self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [self.pedido.items count]];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}



/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    // Navigation logic may go here. Create and push another view controller.
    /*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - alertas

- (void) alertaProblemaEnvioPedido:(NSString*) msgAlerta {
    if ([msgAlerta isEqualToString:@""] || msgAlerta == nil) {
        msgAlerta = [NSString stringWithString:@"Houve um problema ao tentar enviar o pedido. Tente novamente."];
    }
	CustomAlert *alertView = [[CustomAlert alloc] initWithTitle:@"Problema no envio"
														message:msgAlerta
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [CustomAlert setBackgroundColor:[UIColor redColor] 
                    withStrokeColor:[UIColor whiteColor]];
	[alertView show];
}

- (void) alertaPedidoVazio {
	NSString* msg_alert = [NSString stringWithString:@"Não há itens no pedido.\nAdicione pelo menos 1 item antes de enviar."];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Pedido vazio"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}

- (void) alertaPedidoNaoVazio {
	NSString* msg_alert = [NSString stringWithString:@"Há itens no pedido.\nEnvie o pedido ou apague todos os itens."];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Pedido pendente"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}


- (void) alertaItemSelecionadoPendente {
	NSString* msg_alert = [NSString stringWithString:@"Há um item na comanda não adicionado ao pedido.\nAdicione ao pedido ou cancele o item."];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Item pendente na comanda"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}

- (void) alertaPedidoEnviado {
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"comanda";
    } else {
        titulo = @"mesa";
    }

    
    NSString* msg_alert = [NSString stringWithFormat:@"Se deseja confirmar, confira a conta da %@ %@", titulo, self.pedido.mesa];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Pedido enviado"
                                                        message:msg_alert
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void) alertaPedidoEnviadoFalhaImpressao {
    NSString* msg_alert = [NSString stringWithFormat:@"É necessário informar à cozinha que o pedido foi feito."];
    
    
    CustomAlert *alertView = [[CustomAlert alloc] initWithTitle:@"Pedido enviado, mas NÃO impresso"
                                                        message:msg_alert
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [CustomAlert setBackgroundColor:[UIColor redColor] 
                    withStrokeColor:[UIColor whiteColor]];

    [alertView show];

}

- (void) alertaContaAberta {
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"comanda";
    } else {
        titulo = @"mesa";
    }

    NSString* msg_alert = [NSString stringWithFormat:@"Esta %@ está aberta em outro terminal. Tentar novamente?", titulo];
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Mesa Aberta";
    alert.message = msg_alert;
    alert.delegate = self;
    alert.tag = 1;
    [alert addButtonWithTitle:@"Sim"];
    [alert addButtonWithTitle:@"Não"];
    [alert show];
}

- (void) alertaContaBloqueada {
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"comanda";
    } else {
        titulo = @"mesa";
    }

    NSString* msg_alert = [NSString stringWithFormat:@"Esta %@ está bloqueada. Tentar novamente?", titulo];
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Mesa Bloqueada";
    alert.message = msg_alert;
    alert.delegate = self;
    alert.tag = 2;
    [alert addButtonWithTitle:@"Sim"];
    [alert addButtonWithTitle:@"Não"];
    [alert show];
}

- (void) confirmaMesaModoComanda {
    UIAlertView *alert = [[UIAlertView alloc] init];
	alert.title = @"Qual a mesa?";
    alert.message = @"texto escondido.";
	alert.delegate = self;
	alert.tag = 3;
	[alert addButtonWithTitle:@"OK"];
	[alert addButtonWithTitle:@"Cancelar"];
    
    self.txtMesaModoComanda = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 30.0)];
    self.txtMesaModoComanda.backgroundColor = [UIColor whiteColor];
    self.txtMesaModoComanda.keyboardType = UIKeyboardTypeNumberPad;
    self.txtMesaModoComanda.font = [UIFont fontWithName:@"Arial-BoldMT" size:21];
    self.txtMesaModoComanda.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.txtMesaModoComanda.textAlignment = UITextAlignmentCenter;
    [self.txtMesaModoComanda becomeFirstResponder];
    
    [alert addSubview:self.txtMesaModoComanda];
    
	[alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (alertView.tag == 1 || alertView.tag == 2) {
		if (buttonIndex == 0) {
			[self enviaPedido:nil];
		} else if (buttonIndex == 1) {
			[self dismissModalViewControllerAnimated:YES];
		}		
	}
    
    if (alertView.tag == 3) {
        if (buttonIndex == 0) {
            self.lblMsgAguarde1.text = @"Conectando";
            self.viewCarregando.hidden = NO;
            [self enviaPedidoSeWsAtivo];
        }        
    }
}

#pragma mark - Metodos locais

- (void) novaMesa: (id) sender {
    if ([self.pedido estaVazio]) {
        [self dismissModalViewControllerAnimated:YES];
    } else {
        [self alertaPedidoNaoVazio];
    }
}

- (void) enviaPedidoSeWsAtivo {
	[[RKClient sharedClient] get:@"/ativo" delegate:self];
}

- (void) executaEnviaPedido {

    NSString* pedidoStr;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        pedidoStr = [self.pedido escreveLinhaPedido:self.txtMesaModoComanda.text];
    } else {
        pedidoStr = [self.pedido escreveLinhaPedido:nil];
    }
    
    NSLog(@"pedidoStr: %@", pedidoStr);
    
	RKClient* rkClient = [RKClient sharedClient];
	
	if (self.wsAtivo) {
		NSDictionary* params = [NSDictionary dictionaryWithObject:pedidoStr forKey:@"REGISTRO"];  
		[rkClient post:@"/lancamento" params:params delegate:self];
	}
	
}

- (NSDecimalNumber*) calculaSubtotal {
	NSDecimalNumber* subtotal = [NSDecimalNumber zero];    
	for (int i = 0; i < [self.pedido.items count]; i++) {
		ItemPedido* item = [self.pedido.items objectAtIndex:i];
		subtotal = [subtotal decimalNumberByAdding:[item calculaSubtotal]];
	}
	return subtotal;
}

#pragma mark - Metodos IB Action

- (IBAction) enviaPedido: (id) sender {
    if (![self.pedido estaVazio]) {
        if (self.menuVC.itemSelecionado.produto == nil) {
            
            if ([[RKClient sharedClient] isNetworkAvailable]) {
                if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
                    [self confirmaMesaModoComanda];
                } else {
                    self.lblMsgAguarde1.text = @"Conectando";
                    self.viewCarregando.hidden = NO;
                    [self enviaPedidoSeWsAtivo];
                }
            } else {
                self.viewCarregando.hidden = YES;
                NSString* msg_erro = @"Conexão com a rede wireless falhou.";
                [self alertaProblemaEnvioPedido:msg_erro];
            }
        } else {
            [self alertaItemSelecionadoPendente];
            self.tabBarController.selectedIndex = 0;
        }
    } else {
        [self alertaPedidoVazio];
        self.tabBarController.selectedIndex = 0;
    }
}

- (IBAction) editaItem: (id) sender {
	if ([self.btnRemoverItem.titleLabel.text isEqualToString:@"Remover"]) {
		[self.btnRemoverItem setTitle:@"Concluído" forState:UIControlStateNormal];
		[self.tbvItemsPedido setEditing:YES animated:YES];
	} else {
		[self.btnRemoverItem setTitle:@"Remover" forState:UIControlStateNormal];
		[self.tbvItemsPedido setEditing:NO animated:YES];
	}
}

- (IBAction) adicionaItem:(id) sender {
	self.tabBarController.selectedIndex = 0;
}



#pragma mark - RKRequestDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
	if ([request isGET]) {
		if ([response isXML]) {
			NSError *error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
			
			if ([[document.root value] isEqualToString:@"ATIVO"]) {
				self.wsAtivo = YES;
                self.lblMsgAguarde1.text = @"Enviando pedido";
                [self executaEnviaPedido];
			}
		}
	}
	
	if ([request isPOST]) {
		if ([response isXML]) {
			NSError *error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
			
			if (!error) {
				SMXMLElement* resposta_node = document.root;
				NSString* resposta_value = [resposta_node value];
				if ([resposta_value isEqualToString:@"GRAVADO"]) {
                    [self alertaPedidoEnviado];
					[self dismissModalViewControllerAnimated:YES];
				} else if ([resposta_value isEqualToString:@"GRAVADO FALHA IMPRESSAO"]) {
                    [self alertaPedidoEnviadoFalhaImpressao];
					[self dismissModalViewControllerAnimated:YES];
				} else if ([resposta_value isEqualToString:@"CONTA ABERTA"]) {
                    [self alertaContaAberta];
				} else if ([resposta_value isEqualToString:@"CONTA BLOQUEADA"]) {
                    [self alertaContaBloqueada];
				}		
				self.viewCarregando.hidden = YES;
			}
			
		}
	}
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	self.wsAtivo = NO;
	self.viewCarregando.hidden = YES;
    NSString* mensagem_erro = [error localizedDescription];
	NSLog(@"Hit error: %@", error);
	[self alertaProblemaEnvioPedido:mensagem_erro];
}


@end

