//
//  PesquisaProdutoPorCategoriaViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PesquisaProdutoPorCategoriaViewController.h"
#import "AppDelegate.h"
#import "Categoria.h"
#import "MenuViewController.h"
#import "PesquisaProdutoViewController.h"

@implementation PesquisaProdutoPorCategoriaViewController

@synthesize itemSelecionado = _itemSelecionado;
@synthesize mesa = _mesa;
@synthesize categoriasResultsController = _categoriasResultsController;
@synthesize categoriasFiltradas = _categoriasFiltradas;
@synthesize tbvCategorias =_tbvCategorias;
@synthesize menuVC = _menuVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.categoriasFiltradas = [[NSMutableArray alloc] init];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.title = @"Categorias";
    
    
	if (self.categoriasResultsController != nil) {
		return;
	}	
	
	AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext* managedObjectContext = appDelegate.managedObjectContext;
	
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription* categoriaEntity = [NSEntityDescription entityForName:@"Categoria" inManagedObjectContext:managedObjectContext];
	[request setEntity:categoriaEntity];
    
    [NSFetchedResultsController deleteCacheWithName:@"categorias_list.cache"]; 
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"oculto != 'S'"];
	[request setPredicate:predicate];
	
	NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nome" ascending:YES];
	NSArray* sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSFetchedResultsController* fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:@"categorias_list.cache"];
	fetchedResultsController.delegate = self;
	
	NSError *error;
	BOOL success = [fetchedResultsController performFetch:&error];
	if (!success) {
		//Handle the error
	}
	
	/*
	 NSError* error;
	 NSMutableArray* mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
	 
	 if (mutableFetchResults == nil) {
	 //Tratar o erro...
	 }
	 
	 self.items = mutableFetchResults;
	 [mutableFetchResults release];
	 
	 */
	
	self.categoriasResultsController = fetchedResultsController;
    
//    NSLog(@"Qtd de categorias: %d", [[[self.categoriasResultsController sections] objectAtIndex:0] numberOfObjects]);
    
	[self.tbvCategorias reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[self.tbvCategorias reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 1;
    } else {
        return [[self.categoriasResultsController sections] count];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.categoriasFiltradas count];
    } else {
        return [[[self.categoriasResultsController sections] objectAtIndex:section] numberOfObjects];
    }
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    Categoria* categoria = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		categoria = [self.categoriasFiltradas objectAtIndex:indexPath.row];
	} else {
		categoria = [self.categoriasResultsController objectAtIndexPath:indexPath];
	}
	cell.textLabel.text = categoria.nome;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    
    Categoria* categoria = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		categoria = [self.categoriasFiltradas objectAtIndex:indexPath.row];
	} else {
		categoria = [self.categoriasResultsController objectAtIndexPath:indexPath];
	}
    
    PesquisaProdutoViewController* pesquisaProdutoVC = [[PesquisaProdutoViewController alloc] initWithNibName:@"PesquisaProdutoViewController" bundle:nil];
    
    pesquisaProdutoVC.mesa = [NSString stringWithFormat:@"%@", self.mesa];
    pesquisaProdutoVC.itemSelecionado = self.itemSelecionado;
    pesquisaProdutoVC.menuVC = self.menuVC;
    pesquisaProdutoVC.categoriaId = [NSString stringWithFormat:@"%@", categoria.codigo];
    
    [self.navigationController pushViewController:pesquisaProdutoVC animated:YES];

}


#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
	
    [self.categoriasFiltradas removeAllObjects];
	
	Categoria* categoria = nil;
	for (NSUInteger i = 0; i < [[self.categoriasResultsController fetchedObjects] count]; i++) {
		categoria = [self.categoriasResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        // Busca em qualquer parte da string
        //        if ([categoria.nome rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)].location != NSNotFound)
        //        {
        //            //Yes it does contain string
        //            [self.categoriasFiltradas addObject:categoria];
        //        }
        
        // Busca somente no início da string
		NSComparisonResult result = [categoria.nome compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
		if (result == NSOrderedSame)
		{
			[self.categoriasFiltradas addObject:categoria];
		}
	}
	
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	
	[self filterContentForSearchText:searchString scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
	
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];

    // Return YES to cause the search result table view to be reloaded.
    return YES;
	
}





@end
