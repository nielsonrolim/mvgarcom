//
//  LoginBotaoTableViewCell.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginBotaoTableViewCell.h"

@implementation LoginBotaoTableViewCell

@synthesize lblSetor = _lblSetor;
@synthesize txtSetor = _txtSetor;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
