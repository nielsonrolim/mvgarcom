//
//  MesasMapaViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 7/14/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@class EscolherMesaViewController;
@class AppDelegate;

@interface MesasMapaViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIScrollView* scvPrincipal;
@property (nonatomic, strong) IBOutlet UIView* viewCarregando;
@property (nonatomic, strong) EscolherMesaViewController* escolherMesaVC;
@property (nonatomic, strong) NSArray* mesas;
@property (strong, nonatomic) IBOutlet UILabel *lblDesenhadoMapa;
@property (strong, nonatomic) AppDelegate* appDelegate;

@end
