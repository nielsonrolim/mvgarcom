//
//  MenuViewController.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 3/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ItemPedido.h"

@class PedidoViewController;
@class ContaViewController;
@class AppDelegate;

@interface MenuViewController : UIViewController <UITextFieldDelegate, RKRequestDelegate>

@property (nonatomic, strong) IBOutlet UILabel* lblMesa;
@property (nonatomic, strong) IBOutlet UILabel* lblNomeProduto;
@property (nonatomic, strong) IBOutlet UILabel* lblPreco;
@property (nonatomic, strong) IBOutlet UILabel* lblQuantidade;
@property (nonatomic, strong) IBOutlet UITextField* txtBuscaProduto;
@property (nonatomic, strong) IBOutlet UITableView* tbvObservacoes;
@property (nonatomic, strong) IBOutlet UILabel* lblSubTotal;
@property (nonatomic, strong) IBOutlet UIButton* btnOK;
@property (nonatomic, strong) IBOutlet UIButton* btnCancel;
@property (nonatomic, strong) IBOutlet UIButton* btnObservacoes;
@property (nonatomic, strong) IBOutlet UIButton* btnComplementos;
@property (nonatomic, strong) IBOutlet UIControl* ctlDadosProduto;
@property (nonatomic, strong) PedidoViewController* pedidoViewController;
@property (nonatomic, strong) ContaViewController* contaViewController;
@property (nonatomic, strong) ItemPedido* itemSelecionado;
@property (nonatomic, strong) NSDictionary* opcoes;
@property (nonatomic, strong) AppDelegate* appDelegate;

- (IBAction) textFieldDoneEditing: (id) sender;
- (IBAction) buscarProduto: (id) sender;
- (IBAction) adicionaAoPedido: (id)sender;
- (IBAction) cancelaProduto: (id) sender;
- (IBAction) adicionaQuantidade: (id) sender;
- (IBAction) subtraiQuantidade: (id) sender;
- (IBAction) limpaParaEdicao:(id) sender;
- (IBAction) cancelaLimpaParaEdicao:(id) sender;
- (IBAction) abreObservacoes: (id) sender;
- (IBAction) abreComplementos: (id) sender;
- (IBAction) pesquisarProduto: (id) sender;
- (ItemPedido*) configuraItemPedido: (Produto*) produto;
- (NSDecimalNumber*) calculaSubtotal;

@end
