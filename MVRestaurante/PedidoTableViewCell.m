//
//  PedidoTableViewCell.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/10/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import "PedidoTableViewCell.h"
#import "Produto.h"
#import "Obs.h"
#import "Funcoes.h"

@implementation PedidoTableViewCell

@synthesize nome;
@synthesize quantidade;
@synthesize subtotal;
@synthesize btnAdicionaQuantidade;
@synthesize btnSubtraiQuantidade;
@synthesize item;
@synthesize pedidoVC;
@synthesize lblCompl;
@synthesize lblObs;

- (IBAction) adicionaQuantidade: (id) sender {
	item.quantidade = [NSNumber numberWithInt:[item.quantidade intValue] + 1];
	[self reloadData];
	pedidoVC.lblSubTotal.text = [[NSString stringWithFormat:@"R$ %.2f", [[pedidoVC calculaSubtotal] doubleValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];

}
- (IBAction) subtraiQuantidade: (id) sender {
	item.quantidade = [NSNumber numberWithInt:[item.quantidade intValue] - 1];
	if ([item.quantidade intValue] < 1) {
		item.quantidade = [NSNumber numberWithInt:1];
	}
	[self reloadData];
	pedidoVC.lblSubTotal.text = [[NSString stringWithFormat:@"R$ %.2f", [[pedidoVC calculaSubtotal] doubleValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
}

- (void) reloadData {
	if (item) {
		Produto* produto = item.produto;
		
		self.nome.text = produto.nome;
		self.quantidade.text = [item.quantidade stringValue];
		
        self.subtotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.item calculaSubtotal])];
		
        if ([[item outrasObservacoes] isEqualToString:@""]) {
            lblObs.text = [item observacoesToString];
        } else {
            if ([item.observacoesSelecionadas count] > 0) {
                lblObs.text = [[item observacoesToString] stringByAppendingFormat:@", %@",[item outrasObservacoes]];
            } else {
                lblObs.text = [item outrasObservacoes];
            }
        }        
        lblObs.numberOfLines = 0;
        [self.lblObs sizeToFit];
		
        lblCompl.text = [item complementosToString];
        lblCompl.numberOfLines = 0;
        [self.lblCompl sizeToFit];
	}
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}

- (void)willTransitionToState:(UITableViewCellStateMask)state
{
	if ((state & UITableViewCellStateShowingDeleteConfirmationMask) || (state & UITableViewCellStateShowingEditControlMask)) {
		self.quantidade.hidden = YES;
		self.btnAdicionaQuantidade.hidden = YES;
		self.btnSubtraiQuantidade.hidden = YES;
	} else {
		self.quantidade.hidden = NO;
		self.btnAdicionaQuantidade.hidden = NO;
		self.btnSubtraiQuantidade.hidden = NO;		
	}
	[super willTransitionToState:state];
}



@end
