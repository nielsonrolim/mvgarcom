//
//  ConfiguracaoViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;

@interface ConfiguracaoViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField* txtIP;
@property (nonatomic, strong) IBOutlet UITextField* txtPorta;
@property (nonatomic, strong) NSMutableDictionary* configuracao;
@property (nonatomic, strong) IBOutlet UISwitch* swtPesquisaCategoria;
@property (nonatomic, strong) IBOutlet UISwitch* swtModoDemonstracao;
@property (strong, nonatomic) IBOutlet UISwitch *swtMesasPorSetor;
@property (strong, nonatomic) IBOutlet UISwitch *swtBuscaComplementosPorNome;
@property (strong, nonatomic) IBOutlet UISwitch *swtBuscaObservacoesPorNome;
@property (strong, nonatomic) IBOutlet UISwitch *swtModoComanda;
@property (nonatomic, strong) AppDelegate* appDelegate;

- (IBAction) textFieldDoneEditing: (id) sender;
- (IBAction) swtModoDemonstracaoAlterado:(id)sender;

@end
