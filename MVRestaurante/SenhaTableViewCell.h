//
//  SenhaTableViewCell.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SenhaTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UITextField* txtSenha;

@end
