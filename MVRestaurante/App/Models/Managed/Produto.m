//
//  Produto.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/6/11.
//  Copyright (c) 2011 MVarandas. All rights reserved.
//

#import "AppDelegate.h"
#import "Produto.h"
#import "Categoria.h"


@implementation Produto

@dynamic categ;
@dynamic codigo;
@dynamic comanda;
@dynamic complemento;
@dynamic metodo;
@dynamic nome;
@dynamic observ;
@dynamic preco;
@dynamic preco2;
@dynamic preco3;
@dynamic preco4;
@dynamic promocao;
@dynamic produtoId;
@dynamic status;
@dynamic tam;
@dynamic taxa;
@dynamic tipo;
@dynamic categoria;

- (NSDecimalNumber*) preco {

    [self willAccessValueForKey:@"tam"];
    int tam = [[self primitiveValueForKey:@"tam"] intValue];
    [self didAccessValueForKey:@"tam"];

    NSDecimalNumber* preco = [NSDecimalNumber zero];
    
    switch (tam) {
        case 2:
            [self willAccessValueForKey:@"preco2"];
            preco = [self primitiveValueForKey:@"preco2"];
            [self didAccessValueForKey:@"preco2"];
            break;
        case 3:
            [self willAccessValueForKey:@"preco3"];
            preco = [self primitiveValueForKey:@"preco3"];
            [self didAccessValueForKey:@"preco3"];
            break;
        case 4:
            [self willAccessValueForKey:@"preco4"];
            preco = [self primitiveValueForKey:@"preco4"];
            [self didAccessValueForKey:@"preco4"];
            break;
        default:
            // nil, 0 ou 1
            [self willAccessValueForKey:@"preco"];
            preco = [self primitiveValueForKey:@"preco"];
            [self didAccessValueForKey:@"preco"];
            break;
    }
    return preco;
}

- (NSDecimalNumber*) precoComplemento:(NSInteger) tamProduto {
        
    NSDecimalNumber* preco = [NSDecimalNumber zero];
    
    switch (tamProduto) {
        case 2:
            [self willAccessValueForKey:@"preco2"];
            preco = [self primitiveValueForKey:@"preco2"];
            [self didAccessValueForKey:@"preco2"];
            break;
        case 3:
            [self willAccessValueForKey:@"preco3"];
            preco = [self primitiveValueForKey:@"preco3"];
            [self didAccessValueForKey:@"preco3"];
            break;
        case 4:
            [self willAccessValueForKey:@"preco4"];
            preco = [self primitiveValueForKey:@"preco4"];
            [self didAccessValueForKey:@"preco4"];
            break;
        default:
            // nil, 0 ou 1
            [self willAccessValueForKey:@"preco"];
            preco = [self primitiveValueForKey:@"preco"];
            [self didAccessValueForKey:@"preco"];
            break;
    }
    return preco;
}


+ (Produto*) findByCodigoSemPromocaoEmCategoriaNaoOculta:(NSString *)codigo {
	AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	NSManagedObjectContext* managedObjectContext = appDelegate.managedObjectContext;
	
	NSEntityDescription* produtoEntity = [NSEntityDescription entityForName:@"Produto" inManagedObjectContext:managedObjectContext];
	
	NSFetchRequest* produtoRequest = [[NSFetchRequest alloc] init];
	[produtoRequest setEntity:produtoEntity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"codigo == %@ AND promocao != 'S' AND categoria.oculto != 'S'", codigo]];
	[produtoRequest setPredicate:predicate];
	
	NSError* error;
	NSMutableArray* mutableFetchResults = [[managedObjectContext executeFetchRequest:produtoRequest error:&error] mutableCopy];
	
	if ([mutableFetchResults count] == 0) {
		return nil;
	} else {
		Produto* produto = [mutableFetchResults objectAtIndex:0];
		return produto;
	}
}

+ (Produto*) findByCodigo: (NSString*) codigo {
	AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	NSManagedObjectContext* managedObjectContext = appDelegate.managedObjectContext;
	
	NSEntityDescription* produtoEntity = [NSEntityDescription entityForName:@"Produto" inManagedObjectContext:managedObjectContext];
	
	NSFetchRequest* produtoRequest = [[NSFetchRequest alloc] init];
	[produtoRequest setEntity:produtoEntity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"codigo == %@", codigo]];
	[produtoRequest setPredicate:predicate];
	
	NSError* error;
	NSMutableArray* mutableFetchResults = [[managedObjectContext executeFetchRequest:produtoRequest error:&error] mutableCopy];
	
	if ([mutableFetchResults count] == 0) {
		return nil;
	} else {
		Produto* produto = [mutableFetchResults objectAtIndex:0];
		return produto;
	}
}

@end
