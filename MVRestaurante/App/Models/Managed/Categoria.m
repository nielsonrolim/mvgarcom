//
//  Categoria.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Categoria.h"
#import "Categoria.h"
#import "Obs.h"
#import "Produto.h"


@implementation Categoria

@dynamic c_complemento;
@dynamic categoriaId;
@dynamic codigo;
@dynamic e_complemento;
@dynamic fracao;
@dynamic metodo;
@dynamic nome;
@dynamic pede_complemento;
@dynamic pede_observ;
@dynamic peso;
@dynamic tipo;
@dynamic categoria_complemento;
@dynamic observacoes;
@dynamic produtos;
@dynamic oculto;

@end
