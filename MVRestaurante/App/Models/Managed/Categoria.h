//
//  Categoria.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 6/3/11.
//  Copyright 2011 MVarandas. All rights reserved.
//

#import <CoreData/CoreData.h>

@class Obs;
@class Produto;

@interface Categoria :  NSManagedObject  
{
}

@property (nonatomic, strong) NSString * fracao;
@property (nonatomic, strong) NSString * metodo;
@property (nonatomic, strong) NSNumber * c_complemento;
@property (nonatomic, strong) NSString * nome;
@property (nonatomic, strong) NSNumber * codigo;
@property (nonatomic, strong) NSString * tipo;
@property (nonatomic, strong) NSString * pede_observ;
@property (nonatomic, strong) NSString * pede_complemento;
@property (nonatomic, strong) NSString * e_complemento;
@property (nonatomic, strong) NSNumber * categoriaId;
@property (nonatomic, strong) NSString * peso;
@property (nonatomic, strong) Categoria * categoria_complemento;
@property (nonatomic, strong) NSSet* produtos;
@property (nonatomic, strong) NSSet* observacoes;
@property (nonatomic, strong) NSString* oculto;

@end


@interface Categoria (CoreDataGeneratedAccessors)
- (void)addProdutosObject:(Produto *)value;
- (void)removeProdutosObject:(Produto *)value;
- (void)addProdutos:(NSSet *)value;
- (void)removeProdutos:(NSSet *)value;

- (void)addObservacoesObject:(Obs *)value;
- (void)removeObservacoesObject:(Obs *)value;
- (void)addObservacoes:(NSSet *)value;
- (void)removeObservacoes:(NSSet *)value;

@end

