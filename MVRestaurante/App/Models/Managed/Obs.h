//
//  Obs.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/3/11.
//  Copyright 2011 MVarandas. All rights reserved.
//

#import <CoreData/CoreData.h>

@class Categoria;

@interface Obs :  NSManagedObject  
{
}

@property (nonatomic, strong) NSNumber * categ;
@property (nonatomic, strong) NSNumber * cod;
@property (nonatomic, strong) NSString * descri;
@property (nonatomic, strong) NSDecimalNumber * valor;
@property (nonatomic, strong) Categoria * categoria;

+ (Obs*) findByCodigo: (NSString*) codigo AndCategoria: (NSString*) codigo_categoria;

@end
