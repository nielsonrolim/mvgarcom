//
//  Produto.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/6/11.
//  Copyright (c) 2011 MVarandas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Categoria;

@interface Produto : NSManagedObject

@property (nonatomic, strong) NSNumber * categ;
@property (nonatomic, strong) NSNumber * codigo;
@property (nonatomic, strong) NSString * comanda;
@property (nonatomic, strong) NSString * complemento;
@property (nonatomic, strong) NSString * metodo;
@property (nonatomic, strong) NSString * nome;
@property (nonatomic, strong) NSString * observ;
@property (nonatomic, strong) NSDecimalNumber * preco;
@property (nonatomic, strong) NSDecimalNumber * preco2;
@property (nonatomic, strong) NSDecimalNumber * preco3;
@property (nonatomic, strong) NSDecimalNumber * preco4;
@property (nonatomic, strong) NSString* promocao;
@property (nonatomic, strong) NSNumber * produtoId;
@property (nonatomic, strong) NSNumber * status;
@property (nonatomic, strong) NSNumber * tam;
@property (nonatomic, strong) NSString * taxa;
@property (nonatomic, strong) NSNumber * tipo;
@property (nonatomic, strong) Categoria *categoria;

+ (Produto*) findByCodigo: (NSString*) codigo;
+ (Produto*) findByCodigoSemPromocaoEmCategoriaNaoOculta:(NSString *)codigo;
- (NSDecimalNumber*) precoComplemento:(NSInteger) tamProduto;

@end
