// 
//  Obs.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/3/11.
//  Copyright 2011 MVarandas. All rights reserved.
//

#import "AppDelegate.h"
#import "Obs.h"


@implementation Obs 

@dynamic valor;
@dynamic categ;
@dynamic cod;
@dynamic descri;
@dynamic categoria;

+ (Obs*) findByCodigo: (NSString*) codigo AndCategoria: (NSString*) codigo_categoria {
	AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	NSManagedObjectContext* managedObjectContext = appDelegate.managedObjectContext;
	
	NSEntityDescription* produtoEntity = [NSEntityDescription entityForName:@"Obs" inManagedObjectContext:managedObjectContext];
	
	NSFetchRequest* produtoRequest = [[NSFetchRequest alloc] init];
	[produtoRequest setEntity:produtoEntity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cod == %@ AND categ == %@", codigo, codigo_categoria]];
	[produtoRequest setPredicate:predicate];
	
	NSError* error;
	NSMutableArray* mutableFetchResults = [[managedObjectContext executeFetchRequest:produtoRequest error:&error] mutableCopy];
	
	if ([mutableFetchResults count] == 0) {
		return nil;
	} else {
		Obs* obs = [mutableFetchResults objectAtIndex:0];
		return obs;
	}
}


@end
