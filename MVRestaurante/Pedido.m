//
//  Pedido.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 11/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Pedido.h"
#import "ItemPedido.h"
#import "Obs.h"

@implementation Pedido

@synthesize items = _items;
@synthesize operador = _operador;
@synthesize mesa = _mesa;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (NSMutableArray*) items {
    if (_items == nil) {
        _items = [[NSMutableArray alloc] init];
    }
    return _items;
}

- (Garcon*) operador {
    if (_operador == nil) {
        _operador = [[Garcon alloc] init];
    }
    return _operador;
}

- (NSString*) escreveLinhaPedido:(NSString*)mesaModoComanda {
    
    if (!mesaModoComanda) {
        mesaModoComanda = @"0";
    }
    
    NSMutableString* pedidoStr = [NSMutableString stringWithFormat:@"%@;%@;%@;%@;", self.operador.setor, self.mesa, mesaModoComanda, self.operador.numero];
    
    for (ItemPedido* item in self.items) {
		//Produto
		[pedidoStr appendFormat:@"%@;", item.produto.codigo];
		[pedidoStr appendFormat:@"%@;", item.quantidade];
		
		//Complementos
		Produto* compl = nil;
		for (int i = 0; i < 6; i++) {
			if (i < [item.complementosSelecionados count]) {
				compl = [item.complementosSelecionados objectAtIndex:i];
				[pedidoStr appendFormat:@"%@;", compl.codigo];
				[pedidoStr appendFormat:@"%@;", compl.nome];
			} else {
				[pedidoStr appendString:@";"];
				[pedidoStr appendFormat:@";"];
			}
			
		}
		
		//Observaçõs
		Obs* obs = nil;
		for (int i = 0; i < 6; i++) {
			if (i < [item.observacoesSelecionadas count]) {
				obs = [item.observacoesSelecionadas objectAtIndex:i];
				[pedidoStr appendFormat:@"%@;", obs.cod];
				[pedidoStr appendFormat:@"%@;", obs.descri];				
			} else {
				[pedidoStr appendString:@";"];
				[pedidoStr appendFormat:@";"];
			}
		}
		
		//Outras Observações;
		[pedidoStr appendString:[NSString stringWithFormat:@"%@;", item.outrasObservacoes]];
	}
    return (NSString*)pedidoStr;
}

- (BOOL) estaVazio {
    return [self.items count] == 0;
}

@end
