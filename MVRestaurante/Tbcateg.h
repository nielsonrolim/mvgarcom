//
//  Tbcateg.h
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tbcateg : NSObject

@property (nonatomic, strong) NSNumber* identifier;
@property (nonatomic, strong) NSString * fracao;
@property (nonatomic, strong) NSString * metodo;
@property (nonatomic, strong) NSNumber * c_complemento;
@property (nonatomic, strong) NSString * nome;
@property (nonatomic, strong) NSNumber * codigo;
@property (nonatomic, strong) NSString * tipo;
@property (nonatomic, strong) NSString * pede_observ;
@property (nonatomic, strong) NSString * pede_complemento;
@property (nonatomic, strong) NSString * e_complemento;
@property (nonatomic, strong) NSString * peso;
@property (nonatomic, strong) NSString* oculto;

@end
