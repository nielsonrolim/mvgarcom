//
//  Item.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Item.h"
#import "Obs.h"

@implementation Item

@synthesize produto = _produto;
@synthesize quantidade = _quantidade;
@synthesize observacoesSelecionadas = _observacoesSelecionadas;
@synthesize complementosSelecionados = _complementosSelecionados;
@synthesize outrasObservacoes = _outrasObservacoes;


- (NSString*) outrasObservacoes {
    if (!_outrasObservacoes) {
        _outrasObservacoes = @"";
    }
    return _outrasObservacoes;
}

- (NSUInteger) quantidadeDoComplemento:(id)complemento {
    NSUInteger quantidade = 0;
    for (Produto* compl in self.complementosSelecionados) {
        if (compl == complemento) {
            quantidade++;
        }
    }
    return quantidade;
}


- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (NSDecimalNumber*) valorComplementoMaisCaro {
    float maiorValor = 0.0;
    NSInteger tamProduto = [self.produto.tam intValue];
    
    for (Produto* compl in self.complementosSelecionados) {
        if ([[compl precoComplemento:tamProduto] floatValue] > maiorValor) {
            maiorValor = [[compl precoComplemento:tamProduto] floatValue];
        }
    }
    
    return [[NSDecimalNumber alloc] initWithFloat:maiorValor];
}

- (NSDecimalNumber*) valorMedioDosDoisMaioresComplementos {
    NSInteger tamProduto = [self.produto.tam intValue];
    NSArray *sortedArray;
    sortedArray = [self.complementosSelecionados sortedArrayUsingComparator:^(Produto* a, Produto* b) {
        NSDecimalNumber* first = [a precoComplemento:tamProduto];
        NSDecimalNumber* second = [b precoComplemento:tamProduto];
        return [second compare:first];
    }];

    float maior1 = 0.0;
    float maior2 = 0.0;
    float valorMedio = 0.0;
    
    switch ([sortedArray count]) {
        case 0:
            break;
        case 1:
            valorMedio = [[[sortedArray objectAtIndex:0] precoComplemento:tamProduto] floatValue];
            break;
        default:
            maior1 = [[[sortedArray objectAtIndex:0] precoComplemento:tamProduto] floatValue];
            maior2 = [[[sortedArray objectAtIndex:1] precoComplemento:tamProduto] floatValue];            
            valorMedio = (maior1 + maior2) / 2;
            break;
    }

    return [[NSDecimalNumber alloc] initWithFloat:valorMedio];
}

- (NSDecimalNumber*) valorMedioComplementos {
    NSInteger tamProduto = [self.produto.tam intValue];
    int quantidadeComplementos = 0;
    float total = 0.0;
    float valorMedio = 0.0;
    
    for (Produto* compl in self.complementosSelecionados) {
        total = total + [[compl precoComplemento:tamProduto] floatValue];
        quantidadeComplementos++;
    }
    if (quantidadeComplementos > 0) {
        valorMedio = total / quantidadeComplementos;
    }
    
    return [[NSDecimalNumber alloc] initWithFloat:valorMedio];
}

- (NSDecimalNumber*) somaTodosComplementos {
    NSInteger tamProduto = [self.produto.tam intValue];
    float total = 0.0;
    
    for (Produto* compl in self.complementosSelecionados) {
        total = total + [[compl precoComplemento:tamProduto] floatValue];
    }
    
    return [[NSDecimalNumber alloc] initWithFloat:total];
}

- (NSDecimalNumber*) calculaPreco {
    NSDecimalNumber* subtotal = [NSDecimalNumber zero];
    
    // Valor do produto
    subtotal = [subtotal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithDecimal:[self.produto.preco decimalValue]]];
    
    // Valor dos complementos
    NSDecimalNumber* complementosValorTotal = [NSDecimalNumber zero];
    switch ([self.produto.metodo intValue]) {
        case 1:
            // Média dos valores dos complementos
            complementosValorTotal = [self valorMedioComplementos];
            break;
        case 2:
            // Valor do maior complemento
            complementosValorTotal = [self valorComplementoMaisCaro];
            break;
        case 3:
            // Média entre 2 maiores complementos            
            complementosValorTotal = [self valorMedioDosDoisMaioresComplementos];
            break;
        case 4:
            // Soma o valor de todos os complementos ao produto
            complementosValorTotal = [self somaTodosComplementos];            
            break;
        default:
            // nil, 0 ou 1
            // Sem complementos
            break;
    }
    
    subtotal = [subtotal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithDecimal:[complementosValorTotal decimalValue]]];
    
    //Valor das Observações
    for (Obs* obs in self.observacoesSelecionadas) {
        subtotal = [subtotal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithDecimal:[obs.valor decimalValue]]];
    }
    
    return subtotal;
}

- (NSDecimalNumber*) calculaSubtotal {
	NSDecimalNumber* subtotal = [NSDecimalNumber zero];
    
    // Valor do produto
    subtotal = [self calculaPreco];
    
    // Multiplica por Quantidade do item
    subtotal = [subtotal decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithDecimal:[self.quantidade decimalValue]]];
    
	return subtotal;
}

- (NSString*) observacoesToString {
    NSMutableString* tmpText = [NSMutableString stringWithString:@""];
    
    if ([self.observacoesSelecionadas count] >0) {
        tmpText = [NSMutableString stringWithString:@""];
        Obs* obs = nil;
        for (int i = 0; i < [self.observacoesSelecionadas count]; i++) {
            obs = [self.observacoesSelecionadas objectAtIndex:i];
            [tmpText appendFormat:@"%@", obs.descri];
            if (i < ([self.observacoesSelecionadas count]-1)) {
                [tmpText appendString:@", "];
            }
        }
    }
    return tmpText;
}

- (NSString*) complementosToString {
    NSMutableString* tmpText = [NSMutableString stringWithString:@""];
    
    if ([self.complementosSelecionados count] >0) {
        tmpText = [NSMutableString stringWithString:@""];
        Produto* compl = nil;
        for (int i = 0; i < [self.complementosSelecionados count]; i++) {
            compl = [self.complementosSelecionados objectAtIndex:i];
            if ([self quantidadeDoComplemento:compl] > 1) {
                NSRange range = [tmpText rangeOfString: compl.nome];                
                if (range.location == NSNotFound) {
                    [tmpText appendFormat:@"%dX %@", [self quantidadeDoComplemento:compl], compl.nome];
                    if (i < ([self.complementosSelecionados count]-1)) {
                        [tmpText appendString:@", "];
                    }
                }
            } else {
                [tmpText appendFormat:@"%@", compl.nome];
                if (i < ([self.complementosSelecionados count]-1)) {
                    [tmpText appendString:@", "];
                }
            }
        }
    }
    return tmpText;
}

@end
