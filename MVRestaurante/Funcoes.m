//
//  Funcoes.m
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 11/16/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Funcoes.h"

@implementation Funcoes

NSString* formataNumeroComVirgula(NSDecimalNumber* numeroDecimal) {
    return [[NSString stringWithFormat:@"%.2f", [numeroDecimal doubleValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
}

BOOL verificaSessaoExpirada(NSDate* sessaoInicioTimestamp) {
    NSTimeInterval dateTimeNow = [sessaoInicioTimestamp timeIntervalSinceNow] * -1;
    
    float tempoMaximoDaSessao = 6 * 60.00; // Tempo máximo da sessão: 6h
    
    if (dateTimeNow >= tempoMaximoDaSessao) {
        return YES;
    } else {
        return NO;
    }
}

@end
