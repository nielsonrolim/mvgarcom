//
//  MenuViewController.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 3/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "PedidoViewController.h"
#import "ContaViewController.h"
#import "Produto.h"
#import "Categoria.h"
#import "Obs.h"
#import "ObservacoesViewController.h"
#import "OutrasObservacoesViewController.h"
#import "ComplementosViewController.h"
#import "PesquisaProdutoViewController.h"
#import "PesquisaProdutoPorCategoriaViewController.h"
#import "ConstantesConf.h"
#import "Funcoes.h"
#import "SMXMLDocument.h"

@implementation MenuViewController

@synthesize lblMesa = _lblMesa;
@synthesize lblNomeProduto = _lblNomeProduto;
@synthesize lblPreco = _lblPreco;
@synthesize lblQuantidade = _lblQuantidade;
@synthesize txtBuscaProduto = _txtBuscaProduto;
@synthesize tbvObservacoes = _tbvObservacoes;
@synthesize pedidoViewController = _pedidoViewController;
@synthesize contaViewController = _contaViewController;
@synthesize itemSelecionado = _itemSelecionado;
@synthesize lblSubTotal = _lblSubTotal;
@synthesize opcoes = _opcoes;
@synthesize btnOK = _btnOK;
@synthesize btnCancel = _btnCancel;
@synthesize btnObservacoes = _btnObservacoes;
@synthesize btnComplementos = _btnComplementos;
@synthesize ctlDadosProduto = _ctlDadosProduto;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (ItemPedido*) itemSelecionado {
    if (!_itemSelecionado) {
        _itemSelecionado = [[ItemPedido alloc] init];
    }
    return _itemSelecionado;
}

- (void) novaMesa: (id) sender {
    if ([self.pedidoViewController.pedido estaVazio]) {
        [self dismissModalViewControllerAnimated:YES];
    } else {
        [self.pedidoViewController alertaPedidoNaoVazio];
    }
}

- (void) reloadData {
	self.lblNomeProduto.text = self.itemSelecionado.produto.nome;
}

- (IBAction) abreComplementos: (id) sender {

	ComplementosViewController* complementosVC = [[ComplementosViewController alloc] initWithNibName:@"ComplementosViewController" bundle:nil];
	complementosVC.mesa = [NSString stringWithFormat:@"%@", self.lblMesa.text];
	
	complementosVC.itemSelecionado = self.itemSelecionado;
	complementosVC.menuVC = self;
	
	[self.navigationController pushViewController:complementosVC animated:YES];
	
	
}

- (IBAction) abreObservacoes: (id) sender {
		
    if ([self.itemSelecionado.produto.categoria.pede_observ isEqualToString:@"S"]) {
        ObservacoesViewController* observacoesVC = [[ObservacoesViewController alloc] initWithNibName:@"ObservacoesViewController" bundle:nil];
        observacoesVC.mesa = [NSString stringWithFormat:@"%@", self.lblMesa.text];
        
        observacoesVC.itemSelecionado = self.itemSelecionado;
        [self.navigationController pushViewController:observacoesVC animated:YES];
    } else {
        OutrasObservacoesViewController* outrasObservacoesVC = [[OutrasObservacoesViewController alloc] initWithNibName:@"OutrasObservacoesViewController" bundle:nil];
        UINavigationController* outrasObservacoesNavCon = [[UINavigationController alloc] initWithRootViewController:outrasObservacoesVC];
        
        outrasObservacoesVC.itemSelecionado = self.itemSelecionado;
        
        [self presentModalViewController:outrasObservacoesNavCon animated:YES];
    }

	
}

- (IBAction) pesquisarProduto: (id) sender {
    
    [self.txtBuscaProduto resignFirstResponder];
    
    if ([[self.appDelegate.configuracao objectForKey:PESQUISAPORCATEGORIA_KEY] isEqualToString:@"NO"]) {
        PesquisaProdutoViewController* pesquisaProdutoVC = [[PesquisaProdutoViewController alloc] initWithNibName:@"PesquisaProdutoViewController" bundle:nil];
        pesquisaProdutoVC.mesa = [NSString stringWithFormat:@"%@", self.lblMesa.text];
        
        pesquisaProdutoVC.itemSelecionado = self.itemSelecionado;
        pesquisaProdutoVC.menuVC = self;
        
        [self.navigationController pushViewController:pesquisaProdutoVC animated:YES];
    } else {
        PesquisaProdutoPorCategoriaViewController* pesquisaProdutoPorCategoriaVC = [[PesquisaProdutoPorCategoriaViewController alloc] initWithNibName:@"PesquisaProdutoPorCategoriaViewController" bundle:nil];
        pesquisaProdutoPorCategoriaVC.mesa = [NSString stringWithFormat:@"%@", self.lblMesa.text];
        
        pesquisaProdutoPorCategoriaVC.itemSelecionado = self.itemSelecionado;
        pesquisaProdutoPorCategoriaVC.menuVC = self;
        
        [self.navigationController pushViewController:pesquisaProdutoPorCategoriaVC animated:YES];
    }
    
	
	
}

- (IBAction) adicionaQuantidade: (id) sender {
	if (![self.txtBuscaProduto.text isEqual:@""]) {
		self.itemSelecionado.quantidade = [NSNumber numberWithInt:[self.itemSelecionado.quantidade intValue] + 1];
		self.lblQuantidade.text = [self.itemSelecionado.quantidade stringValue];
		self.lblSubTotal.text = formataNumeroComVirgula([self calculaSubtotal]);
	}
}

- (IBAction) subtraiQuantidade: (id) sender {
	if (![self.txtBuscaProduto.text isEqual:@""]) {
		self.itemSelecionado.quantidade = [NSNumber numberWithInt:[self.itemSelecionado.quantidade intValue] - 1];
		if ([self.itemSelecionado.quantidade intValue] < 1) {
			self.itemSelecionado.quantidade = [NSNumber numberWithInt:1];
		}
		self.lblQuantidade.text = [self.itemSelecionado.quantidade stringValue];
		self.lblSubTotal.text = formataNumeroComVirgula([self calculaSubtotal]);
	}
}

- (NSDecimalNumber*) calculaSubtotal {
	return [self.itemSelecionado calculaSubtotal];
}

- (void) limpaCamposDoProduto {
    self.itemSelecionado = nil;
	self.txtBuscaProduto.text = @"";
	self.lblNomeProduto.text = @"";
	self.lblPreco.text = @"";
	self.lblQuantidade.text = @"0";
	self.lblSubTotal.text = @"";
	self.btnOK.hidden = NO;
	self.btnCancel.hidden = YES;
	self.ctlDadosProduto.hidden = YES;
}

- (IBAction) limpaParaEdicao:(id) sender {
	self.btnOK.hidden = NO;
	self.btnCancel.hidden = YES;
}

- (IBAction) cancelaLimpaParaEdicao:(id) sender {
	if (self.itemSelecionado.produto != nil) {
		self.btnOK.hidden = YES;
		self.btnCancel.hidden = NO;
		[self.txtBuscaProduto setText:[NSString stringWithFormat:@"%@", self.itemSelecionado.produto.codigo]];
	} else {
		self.txtBuscaProduto.text = @"";
	}
}

- (BOOL) codigoProdutoEmBranco {
	BOOL emBranco = NO;
	
	if ([self.txtBuscaProduto.text isEqualToString:@""]) {
		
		[self limpaCamposDoProduto];
		
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Código não informado"
															message:@"Para digitar o código clique no campo"
														   delegate:nil
												  cancelButtonTitle:@"OK"
												  otherButtonTitles:nil];
		[alertView show];
		emBranco = YES;
	}
	return emBranco;
}

- (IBAction) cancelaProduto: (id) sender {
	[self limpaCamposDoProduto];
}

- (IBAction) adicionaAoPedido: (id)sender {
	
	if ([self codigoProdutoEmBranco]) {
		return;
	}
	    
	[self.pedidoViewController.pedido.items addObject:self.itemSelecionado];
	
	self.pedidoViewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [self.pedidoViewController.pedido.items count]];
	
	[self limpaCamposDoProduto];
}

- (IBAction) textFieldDoneEditing: (id) sender {
	[self.txtBuscaProduto resignFirstResponder];
}

- (ItemPedido*) configuraItemPedido: (Produto*) produto {
	ItemPedido* novoItemPedido = [[ItemPedido alloc] init];
	novoItemPedido.produto = produto;
	novoItemPedido.quantidade = [NSNumber numberWithInt:1];
	
	self.itemSelecionado = novoItemPedido;
	
	self.lblNomeProduto.text = produto.nome;
    self.lblPreco.text = formataNumeroComVirgula([self.itemSelecionado calculaPreco]);
	self.lblQuantidade.text = [novoItemPedido.quantidade stringValue];
    self.lblSubTotal.text = formataNumeroComVirgula([self calculaSubtotal]);
	self.txtBuscaProduto.text = [produto.codigo stringValue];
	
	
//	if ([produto.categoria.pede_observ isEqualToString:@"S"]) {
//		self.btnObservacoes.hidden = NO;
//	} else {
//		self.btnObservacoes.hidden = YES;
//	}
    
//    [[[self.categoriasResultsController sections] objectAtIndex:0] numberOfObjects]
    
    NSLog(@"Qtd de categorias: %d", [self.itemSelecionado.produto.categoria.categoria_complemento.produtos count]);
    
	if ([self.itemSelecionado.produto.categoria.categoria_complemento.produtos count] > 0) {
		self.btnComplementos.hidden = NO;
	} else {
		self.btnComplementos.hidden = YES;
	}
	
	
	self.btnOK.hidden = YES;
	self.btnCancel.hidden = NO;
	self.ctlDadosProduto.hidden = NO;
	
	return novoItemPedido;
}

- (IBAction) buscarProduto:(id) sender {
	if ([self codigoProdutoEmBranco]) {
		return;
	}	
		
	Produto* produto = [Produto findByCodigoSemPromocaoEmCategoriaNaoOculta:self.txtBuscaProduto.text];
	
	if (produto == nil) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Produto não encontrado"
															message:@"Digite um outro código"
														   delegate:nil
												  cancelButtonTitle:@"OK"
												  otherButtonTitles:nil];
		[alertView show];
		
		[self limpaCamposDoProduto];		
	} else {
		[self configuraItemPedido:produto];
	}
	
	[self.txtBuscaProduto resignFirstResponder];
}

#pragma mark -
#pragma mark Initialization

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization.
		self.title = @"Comanda";
		
		UIImage* anImage = [UIImage imageNamed:@"page.png"];
		UITabBarItem* theItem = [[UITabBarItem alloc] initWithTitle:@"Comanda" image:anImage tag:0];
		self.tabBarItem = theItem;
	}
	return self;
}



#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
	[super viewDidLoad];
	
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"Comanda";
    } else {
        titulo = @"Mesa";
    }
    
	// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	// self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Nova %@", titulo] style:UIBarButtonItemStylePlain target:self action:@selector(novaMesa:)];
	
    self.lblMesa.text = [NSString stringWithFormat:@"%@ %@", titulo, self.pedidoViewController.pedido.mesa];	
	
	self.pedidoViewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [self.pedidoViewController.pedido.items count]];
	
//	[txtBuscaProduto becomeFirstResponder];
}



- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

    NSDictionary* params = [NSDictionary dictionaryWithObject:self.pedidoViewController.pedido.mesa forKey:@"MESA"];  
	[[RKClient sharedClient] post:@"/conta" params:params delegate:self];
}


/*
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}
*/

/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/

/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

//Textfield methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 4);
}

#pragma mark - RKRequestDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
	if ([request isPOST]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
            
            if ([document.root.name isEqualToString:@"tbConta"]) {
//                NSMutableArray* itensTmp = [[NSMutableArray alloc] init];
                
                self.contaViewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [[document.root childrenNamed:@"conta"] count]];
            }
        }
//        self.viewCarregando.hidden = YES;
	}
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
//	self.viewCarregando.hidden = YES;
    NSString* mensagem_erro = [error localizedDescription];
	NSLog(@"Hit error: %@", mensagem_erro);
//	[self alertaProblemaEnvioPedido:mensagem_erro];
}



@end
