//
//  CancelaItensPorGarcomTableViewCell.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemCancelamento.h"

@class CancelaItensPorGarcomViewController;

@interface CancelaItensPorGarcomTableViewCell : UITableViewCell

@property (nonatomic, strong) CancelaItensPorGarcomViewController* cancelaItensPorGarcomVC;
@property (nonatomic, strong) ItemCancelamento* item;
@property (nonatomic, assign) NSUInteger linha;
@property (nonatomic, strong) IBOutlet UILabel* nome;
@property (nonatomic, strong) IBOutlet UILabel* lblQuantidadeACancelar;
@property (nonatomic, strong) IBOutlet UIButton* btnAdicionaQuantidade;
@property (nonatomic, strong) IBOutlet UIButton* btnSubtraiQuantidade;
@property (nonatomic, strong) IBOutlet UILabel* lblCompl;
@property (nonatomic, strong) IBOutlet UILabel* lblObs;
@property (nonatomic, strong) IBOutlet UILabel* lblQuantidade;
@property (nonatomic, strong) IBOutlet UILabel* lblNomeGarcom;

- (IBAction) adicionaQuantidade: (id) sender;
- (IBAction) subtraiQuantidade: (id) sender;
- (void) reloadData;

@end
