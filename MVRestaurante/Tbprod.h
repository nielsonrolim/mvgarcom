//
//  Tbprod.h
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 10/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>

@interface Tbprod : NSObject

@property (nonatomic, strong) NSNumber* identifier;
@property (nonatomic, strong) NSNumber* codigo;
@property (nonatomic, strong) NSString* nome;
@property (nonatomic, strong) NSDecimalNumber* preco;
@property (nonatomic, strong) NSDecimalNumber* preco2;
@property (nonatomic, strong) NSDecimalNumber* preco3;
@property (nonatomic, strong) NSDecimalNumber* preco4;
@property (nonatomic, strong) NSString* promocao;
@property (nonatomic, strong) NSNumber* categ;
@property (nonatomic, strong) NSNumber* tipo;
@property (nonatomic, strong) NSString* comanda;
@property (nonatomic, strong) NSString* taxa;
@property (nonatomic, strong) NSString* complemento;
@property (nonatomic, strong) NSString* observ;
@property (nonatomic, strong) NSString* metodo;
@property (nonatomic, strong) NSNumber* tam;
@property (nonatomic, strong) NSNumber* status;

@end
