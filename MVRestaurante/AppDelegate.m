//
//  AppDelegate.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "InicioViewController.h"
#import "Tbprod.h"
#import "Tbcateg.h"
#import "Tbobs.h"
#import "ConstantesConf.h"


@implementation AppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize inicioViewController = _inicioViewController;

@synthesize operador = _operador;
@synthesize configuracaoURL = _configuracaoURL;
@synthesize configuracao = _configuracao;

- (NSURL*) configuracaoURL {
    _configuracaoURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ConfiguracaoMVRestaurante.plist"];
    return _configuracaoURL;
}

- (NSMutableDictionary*) configuracao {
    _configuracao = [[NSMutableDictionary alloc] initWithContentsOfURL:self.configuracaoURL];
    return _configuracao;
}

- (RKObjectManager*) configuraWebServiceComIp:(NSString*)enderecoIP porta:(NSString*)porta {
    
    NSString* serverURL = [NSString stringWithFormat:@"http://%@:%@/mvarandas/mvws.exe", enderecoIP, porta];
    
	// Initialize RestKit
	RKClient* client = [RKClient clientWithBaseURL:serverURL];
	RKObjectManager* objectManager = [RKObjectManager objectManagerWithBaseURL:serverURL];
	
    // Enable automatic network activity indicator management
    [RKRequestQueue sharedQueue].showsNetworkActivityIndicatorWhenBusy = YES;
	
    [client isNetworkAvailable];
    
    return objectManager;
}

- (void) configuraMapeamento:(RKObjectManager*) objectManager {
    // Uncomment this to use XML, comment it to use JSON
    objectManager.acceptMIMEType = RKMIMETypeXML;	
    
    RKObjectMapping* produtoMapping = [RKObjectMapping mappingForClass:[Tbprod class]];
    [produtoMapping mapKeyPathsToAttributes:
     @"id", @"identifier",
     @"codigo", @"codigo",
     @"nome", @"nome",
     @"preco", @"preco",
     @"preco2", @"preco2",
     @"preco3", @"preco3",
     @"preco4", @"preco4",
     @"promocao", @"promocao",
     @"categ", @"categ",
     @"tipo", @"tipo",
     @"comanda", @"comanda",
     @"taxa", @"taxa",
     @"complemento", @"complemento",
     @"observ", @"observ",
     @"metodo", @"metodo",
     @"tam", @"tam",
     @"status", @"status",
     nil];
    // Ex. Relacionamento
    //    [statusMapping mapRelationship:@"user" withMapping:userMapping];
    
    RKObjectMapping* categoriaMapping = [RKObjectMapping mappingForClass:[Tbcateg class]];
    [categoriaMapping mapKeyPathsToAttributes:
     @"id", @"identifier",
     @"codigo", @"codigo",
     @"nome", @"nome",
     @"tipo", @"tipo",
     @"pede-complemento", @"pede_complemento",
     @"pede-observ", @"pede_observ",
     @"metodo", @"metodo",
     @"fracao", @"fracao",
     @"peso", @"peso",
     @"c-complemento", @"c_complemento",
     @"e-complemento", @"e_complemento",
     @"oculto", @"oculto",
     nil];
    
    RKObjectMapping* observacoesMapping = [RKObjectMapping mappingForClass:[Tbobs class]];
    [observacoesMapping mapKeyPathsToAttributes:
     @"codigo", @"codigo",
     @"categ", @"categ",
     @"descri", @"descri",
     @"valor", @"valor",
     nil];
    
    [objectManager.mappingProvider setMapping:produtoMapping forKeyPath:@"tbprods.tbprod"];
    [objectManager.mappingProvider setMapping:categoriaMapping forKeyPath:@"tbcategs.tbcateg"];
    [objectManager.mappingProvider setMapping:observacoesMapping forKeyPath:@"tbobserv.tbobs"];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSMutableDictionary* conf = self.configuracao;
    
    if (conf == nil) {
        conf = [[NSMutableDictionary alloc] init];
        [conf setObject:@"YES" forKey:PESQUISAPORCATEGORIA_KEY];
        [conf setObject:@"YES" forKey:BUSCACOMPLEMENTOSPORNOME_KEY];
        [conf setObject:@"YES" forKey:BUSCAOBSERVACOESPORNOME_KEY];
        [conf setObject:@"YES" forKey:MESASPORSETOR_KEY];
        [conf setObject:@"YES" forKey:MODODEMONSTRACAO_KEY];
        [conf setObject:@"NO" forKey:MODOCOMANDA_KEY];
        
        [conf writeToURL:self.configuracaoURL atomically:YES];
    }
    
    RKObjectManager* objectManager = nil;
    
    if ([[conf objectForKey:MODODEMONSTRACAO_KEY] isEqualToString:@"YES"] || [conf count] == 0) {
        objectManager = [self configuraWebServiceComIp:DEMOENDERECO porta:DEMOPORTA];
    } else {
        objectManager = [self configuraWebServiceComIp:[conf objectForKey:ENDERECO_IP_KEY] porta:[conf objectForKey:PORTA_KEY]];
    }
    
    [self configuraMapeamento:objectManager];
        
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    // Test if it's a iPhone
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.inicioViewController = [[InicioViewController alloc] initWithNibName:@"InicioViewController" bundle:nil];
    } else {
        self.inicioViewController = [[InicioViewController alloc] initWithNibName:@"InicioViewController_iPad" bundle:nil];
    }
    self.window.rootViewController = self.inicioViewController;
//    self.mainViewController.managedObjectContext = self.managedObjectContext;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MVRestaurante" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MVRestaurante.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
