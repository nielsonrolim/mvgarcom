//
//  ObservacoesViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/17/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPedido.h"

@interface ObservacoesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate>

@property (nonatomic, strong) NSString* mesa;
@property (nonatomic, strong) NSMutableArray* selectedArray;
@property (nonatomic, strong) NSMutableArray* observacoes;
@property (nonatomic, strong) NSMutableArray* observacoesFiltradas;
@property (nonatomic, strong) ItemPedido* itemSelecionado;
@property (nonatomic, strong) IBOutlet UITableView* tbvObservacoes;
@property (nonatomic, strong) NSMutableDictionary* configuracao;
@property (strong, nonatomic) IBOutlet UISearchBar *sbarBusca;

- (IBAction) done: (id) sender;
- (IBAction) abreOutrasObservacoes:(id)sender;
NSInteger comparaObs(id arg1, id arg2, void *arg3);

@end
