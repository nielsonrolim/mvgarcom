//
//  LoginBotaoTableViewCell.h
//  MVRestaurante
//
//  Created by Nielson Rolim on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginBotaoTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel* lblSetor;
@property (nonatomic, strong) IBOutlet UITextField* txtSetor;

@end
