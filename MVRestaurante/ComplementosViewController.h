//
//  ComplementosViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/16/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPedido.h"

@class MenuViewController;
@class ComplementoTableViewCell;

@interface ComplementosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate>

@property (nonatomic, strong) NSString* mesa;
@property (nonatomic, strong) NSMutableArray* complementos;
@property (nonatomic, strong) NSMutableArray* complementosFiltrados;
@property (nonatomic, strong) NSMutableArray* selectedArray;
@property (nonatomic, strong) ItemPedido* itemSelecionado;
@property (nonatomic, strong) IBOutlet UITableView* tbvComplementos;
@property (nonatomic, strong) MenuViewController* menuVC;
@property (nonatomic, strong) NSMutableDictionary* configuracao;
@property (strong, nonatomic) IBOutlet UISearchBar *sbarBusca;

NSInteger comparaComplementos(id arg1, id arg2, void *arg3);
- (IBAction) done: (id) sender;
- (void) adicionaComplementoAosSelecionado:(id)complemento;
- (void) removeComplementoDosSelecionados:(id)complemento;
- (NSUInteger) quantidadeDoComplemento:(id)complemento;

@end
