//
//  OutrasObservacoesViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OutrasObservacoesViewController.h"

@implementation OutrasObservacoesViewController

@synthesize itemSelecionado = _itemSelecionado;
@synthesize txvObservacoes = _txvObservacoes;

- (IBAction) textFieldDoneEditing: (id) sender {
//    [txtPorta resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self textFieldDoneEditing:nil];
    return YES;
}

- (IBAction) salvar:(id)sender {
    self.itemSelecionado.outrasObservacoes = self.txvObservacoes.text;
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction) cancelar:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = [NSString stringWithString:@"Outras Observações"];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(cancelar:)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Salvar" style:UIBarButtonItemStylePlain target:self action:@selector(salvar:)];

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if (self.itemSelecionado.outrasObservacoes != nil) {
        self.txvObservacoes.text = self.itemSelecionado.outrasObservacoes;
    }
    [self.txvObservacoes becomeFirstResponder];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//Textview methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    return !([newString length] > 30);
}


@end
