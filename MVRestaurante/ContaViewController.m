//
//  ContaViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 7/18/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import "ContaViewController.h"
#import "ContaTableViewCell.h"
#import "Produto.h"
#import "Obs.h"
#import "ItemConta.h"
#import "SMXMLDocument.h"
#import "PedidoViewController.h"
#import "CustomAlert.h"
#import "Funcoes.h"
#import "ConstantesConf.h"

@interface ContaViewController ()
@property (nonatomic, strong) UITextField* txtQuantidadeDePessoasNaMesa;
@end

@implementation ContaViewController

@synthesize conta = _conta;
@synthesize pedidoViewController = _pedidoViewController;
@synthesize lblMesa = _lblMesa;
@synthesize tbvConta =  _tbvConta;
@synthesize lblSubtotal =_lblSubtotal;
@synthesize lblTaxaServico = _lblTaxaServico;
@synthesize lblTotal = _lblTotal;
@synthesize viewCarregando = _viewCarregando;
@synthesize lblMsgAguarde = _lblMsgAguarde;
@synthesize wsAtivo = _wsAtivo;
@synthesize txtQuantidadeDePessoasNaMesa = _txtQuantidadeDePessoasNaMesa;
@synthesize appDelegate = _appDelegate;

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

#pragma mark -
#pragma mark View lifecycle

/*
 - (id)init {
 if (self = [super initWithNibName:@"ContaViewController" bundle:nil]) {
 self.title = @"Conta";
 
 UIImage* anImage = [UIImage imageNamed:@"pencilangled.png"];
 UITabBarItem* theItem = [[UITabBarItem alloc] initWithTitle:@"Conta" image:anImage tag:0];
 self.tabBarItem = theItem;
 [theItem release];
 }
 return self;
 }
 */

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization.
		self.title = @"Conta";
		
		UIImage* anImage = [UIImage imageNamed:@"pencilangled.png"];
		UITabBarItem* theItem = [[UITabBarItem alloc] initWithTitle:@"Conta" image:anImage tag:0];
		self.tabBarItem = theItem;
        
        self.conta = [[Conta alloc] init];
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"Comanda";
    } else {
        titulo = @"Mesa";
    }
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Nova %@", titulo] style:UIBarButtonItemStylePlain target:self action:@selector(novaMesa:)];
	self.lblMesa.text = [NSString stringWithFormat:@"%@ %@", titulo, self.conta.mesa];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.lblMsgAguarde.text = @"Carregando conta";
    self.viewCarregando.hidden = NO;
    
	NSDictionary* params = [NSDictionary dictionaryWithObject:self.conta.mesa forKey:@"MESA"];  
	[[RKClient sharedClient] post:@"/conta" params:params delegate:self];
}


/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.conta.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	ItemConta* itemConta = (ItemConta*)[self.conta.items objectAtIndex:indexPath.row];
	
	if ( ([itemConta.complementosSelecionados count] > 0) || ([itemConta.observacoesSelecionadas count] > 0 || ![itemConta.outrasObservacoes isEqualToString:@""]) ) {
		CGFloat altura;
		altura = 105;
		int linhasAdicionais = [itemConta.complementosSelecionados count] / 5;
		for (int i=0; i < linhasAdicionais; i++) {
			altura += 70;
		}
		return altura;
	} else {
		return 55;
	}
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	ItemConta* itemConta = (ItemConta*)[self.conta.items objectAtIndex:indexPath.row];
	BOOL temComplementos = NO;
	BOOL temObservacoes = NO;
    	
	if ([itemConta.observacoesSelecionadas count] > 0 || ![itemConta.outrasObservacoes isEqualToString:@""]) {
		temObservacoes = YES;
	}
	
	if ([itemConta.complementosSelecionados count] > 0) {
		temComplementos = YES;
	}
	
    static NSString* CellIdentifier = @"contaCell";
	
	ContaTableViewCell* cell = (ContaTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	//	if (cell == nil){
	//	}
	
	NSArray* topLevelObjects = [NSArray array];
	
	if (temObservacoes || temComplementos) {
		if (temObservacoes && temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContaComplObsTableViewCell" owner:nil options:nil];
		} else if (temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContaComplTableViewCell" owner:nil options:nil];
		} else if (temObservacoes) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContaObsTableViewCell" owner:nil options:nil];
		}
		
	} else {
		topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContaTableViewCell" owner:nil options:nil];				
	}
	
	for(id currentObject in topLevelObjects) {			
		if([currentObject isKindOfClass:[ContaTableViewCell class]]) {
			cell = (ContaTableViewCell *)currentObject;
			break;
		}
	}
	
	cell.item = itemConta;
	cell.quantidade.hidden = NO;
	[cell reloadData];
	
	return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

#pragma mark - alertas

- (void) alertaProblemaEnvioPedido:(NSString*) msgAlerta; {
    if ([msgAlerta isEqualToString:@""] || msgAlerta == nil) {
        msgAlerta = [NSString stringWithString:@"Houve um problema ao tentar enviar o pedido. Tente novamente."];
    }
	CustomAlert *alertView = [[CustomAlert alloc] initWithTitle:@"Problema no envio"
														message:msgAlerta
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [CustomAlert setBackgroundColor:[UIColor redColor] 
                    withStrokeColor:[UIColor whiteColor]];
	[alertView show];
}

- (void) alertaContaImpressa {
    NSString* titulo;
    if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
        titulo = @"comanda";
    } else {
        titulo = @"mesa";
    }

    NSString* msg_alert = [NSString stringWithFormat:@"A conta da %@ %@ foi impressa com sucesso.", titulo, self.conta.mesa];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Conta impressa"
                                                        message:msg_alert
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void) alertaVermelhoComTitulo:(NSString*)titulo mensagem:(NSString*)msg {
    if ([titulo isEqualToString:@""] || titulo == nil) {
        titulo = @"Não Impresso";
    }
    if ([msg isEqualToString:@""] || msg == nil) {
        msg = @"Houve um problema ao tentar imprimir a conta. Tente novamente.";
    }
	CustomAlert *alertView = [[CustomAlert alloc] initWithTitle:titulo
														message:msg
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [CustomAlert setBackgroundColor:[UIColor redColor] 
                    withStrokeColor:[UIColor whiteColor]];
	[alertView show];
}

- (void) confirmaQuantidadePessoasNaMesa {
    UIAlertView *alert = [[UIAlertView alloc] init];
	alert.title = @"Quantas pessoas?";
    alert.message = @"texto escondido.";
	alert.delegate = self;
	alert.tag = 1;
	[alert addButtonWithTitle:@"OK"];
	[alert addButtonWithTitle:@"Cancelar"];

    self.txtQuantidadeDePessoasNaMesa = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 30.0)];
    self.txtQuantidadeDePessoasNaMesa.backgroundColor = [UIColor whiteColor];
    self.txtQuantidadeDePessoasNaMesa.keyboardType = UIKeyboardTypeNumberPad;
    self.txtQuantidadeDePessoasNaMesa.font = [UIFont fontWithName:@"Arial-BoldMT" size:21];
    self.txtQuantidadeDePessoasNaMesa.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.txtQuantidadeDePessoasNaMesa.textAlignment = UITextAlignmentCenter;
    [self.txtQuantidadeDePessoasNaMesa becomeFirstResponder];
    
    [alert addSubview:self.txtQuantidadeDePessoasNaMesa];
    
	[alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{ 
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            self.lblMsgAguarde.text = @"Conectando";
            self.viewCarregando.hidden = NO;
            [self imprimeContaSeWsAtivo];
        }
    }
}


#pragma mark - Metodos locais

- (void) novaMesa: (id) sender {
    if ([self.pedidoViewController.pedido estaVazio]) {
        [self dismissModalViewControllerAnimated:YES];
    } else {
        [self.pedidoViewController alertaPedidoNaoVazio];
    }
}

- (void) imprimeContaSeWsAtivo {	
    [[RKClient sharedClient] get:@"/ativo" delegate:self];
}

- (void) executaImprimeConta {
	RKClient* rkClient = [RKClient sharedClient];
	if (self.wsAtivo) {
        Garcon* operador = self.appDelegate.operador;
        NSString* strImprimeConta = [NSString stringWithFormat:@"%@;%@;@", self.conta.mesa, self.txtQuantidadeDePessoasNaMesa.text, operador.numero];
        NSDictionary* params = [NSDictionary dictionaryWithObject:strImprimeConta forKey:@"MESA"];  
        [rkClient post:@"/imprime" params:params delegate:self];
	}
}

#pragma mark - Metodos IB Action

- (IBAction) imprimeConta: (id) sender {
    if (![self.conta estaVazio]) {
        if ([[RKClient sharedClient] isNetworkAvailable]) {
            self.lblMsgAguarde.text = @"Conectando";
            self.viewCarregando.hidden = NO;
            if ([[self.appDelegate.configuracao objectForKey:MODOCOMANDA_KEY] isEqualToString:@"YES"]) {
                [self imprimeContaSeWsAtivo];
            } else {
                [self confirmaQuantidadePessoasNaMesa];
            }
        } else {
            self.viewCarregando.hidden = YES;
            NSString* msg_erro = @"Conexão com a rede wireless falhou.";
            [self alertaProblemaEnvioPedido:msg_erro];
        }
    }
}

#pragma mark - RKRequestDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
    if ([request isGET]) {
		if ([response isXML]) {
			NSError *error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
			
			if ([document.root.value isEqualToString:@"ATIVO"]) {
				self.wsAtivo = YES;
                self.lblMsgAguarde.text = @"Imprimindo conta";
                [self executaImprimeConta];
			}
		}
	}
    
	if ([request isPOST]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];

            if ([document.root.name isEqualToString:@"tbConta"]) {
                NSMutableArray* itensTmp = [[NSMutableArray alloc] init];
                
                for (SMXMLElement* itemXml in [document.root childrenNamed:@"conta"] ) {
                    
                    NSString* codigoObs = [itemXml valueWithPath:@"obs1"];
                    
                    if (codigoObs != nil) {
                        ItemConta* itemTmp = [[ItemConta alloc] init];
                        NSMutableArray* observacoesSelecionadasTmp = [[NSMutableArray alloc] init];
                        NSMutableArray* complementosSelecionadosTmp = [[NSMutableArray alloc] init];
                        
                        itemTmp.produto = [Produto findByCodigo:[itemXml valueWithPath:@"codigo"]];
                        itemTmp.quantidade = [NSNumber numberWithDouble:[[itemXml valueWithPath:@"quant"] doubleValue]];
                        
                        for (int i = 1; i <= 6; i++) {
                            codigoObs = [itemXml valueWithPath:[NSString stringWithFormat:@"obs%d", i]];
                            if (![codigoObs isEqualToString:@"0"]) {
                                [observacoesSelecionadasTmp addObject:[Obs findByCodigo:codigoObs AndCategoria:[itemTmp.produto.categ stringValue]]];
                            }
                        }
                        
                        NSString* codigoCompl;
                        for (int i = 1; i <= 6; i++) {
                            codigoCompl = [itemXml valueWithPath:[NSString stringWithFormat:@"parte%d", i]];
                            if (![codigoCompl isEqualToString:@"0"]) {
                                [complementosSelecionadosTmp addObject:[Produto findByCodigo:codigoCompl]];
                            }
                        }

                        
                        itemTmp.observacoesSelecionadas = observacoesSelecionadasTmp;
                        
                        itemTmp.complementosSelecionados = complementosSelecionadosTmp;
                        
                        itemTmp.outrasObservacoes = [itemXml valueWithPath:@"observacao"];
                        
                        [itensTmp addObject:itemTmp];
                        
                    }				
                }
                self.conta.items = itensTmp;			
                                
                [self.tbvConta reloadData];
                
                self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [self.conta.items count]];
            } else if ([document.root.value isEqualToString:@"IMPRESSO"]) {
                [self alertaContaImpressa];
            } else if ([document.root.value isEqualToString:@"CONTA FECHADA"]) {
                [self alertaVermelhoComTitulo:@"Conta Fechada" mensagem:nil];
            } else if ([document.root.value isEqualToString:@"CONTA ABERTA"]) {
                [self alertaVermelhoComTitulo:@"Mesa Aberta" mensagem:nil];
            } else if ([document.root.value isEqualToString:@"CONTA EMITIDA"]) {
                [self alertaVermelhoComTitulo:@"Conta Já Emitida" mensagem:nil];
            }
        }
        self.lblSubtotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.conta subtotal])];
        self.lblTaxaServico.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.conta taxaDeServico])];
        self.lblTotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.conta total])];
        self.viewCarregando.hidden = YES;
	}
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	self.wsAtivo = NO;
	self.viewCarregando.hidden = YES;
    NSString* mensagem_erro = [error localizedDescription];
	NSLog(@"Hit error: %@", error);
	[self alertaProblemaEnvioPedido:mensagem_erro];
}


@end

