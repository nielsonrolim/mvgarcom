//
//  LoginViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@class AppDelegate;

@interface LoginViewController : UITableViewController <RKRequestDelegate>

@property (nonatomic, strong) NSArray* garcons;
@property (nonatomic, assign) int garconSelecionadoId;
@property (nonatomic, assign) BOOL dropDownOpen;
@property (nonatomic, assign) BOOL wsAtivo;
@property (nonatomic, strong) UITextField* txtSenha;
@property (nonatomic, strong) UITextField* txtSetor;
@property (nonatomic, strong) AppDelegate* appDelegate;

- (void) carregaGarcon;

NSInteger comparaGarcons(id arg1, id arg2, void *arg3);

- (void) executaLoginSeWSAtivo;

@end
