//
//  ComplementosViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/16/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import "AppDelegate.h"
#import "ConstantesConf.h"
#import "ComplementosViewController.h"
#import "Categoria.h"
#import "Produto.h"
#import "MenuViewController.h"
#import "Funcoes.h"
#import "ComplementoTableViewCell.h"

@implementation ComplementosViewController

@synthesize mesa = _mesa;
@synthesize selectedArray = _selectedArray;
@synthesize complementos = _complementos;
@synthesize complementosFiltrados = _complementosFiltrados;
@synthesize itemSelecionado = _itemSelecionado;
@synthesize tbvComplementos = _tbvComplementos;
@synthesize menuVC = _menuVC;
@synthesize configuracao = _configuracao;
@synthesize sbarBusca = _sbarBusca;

#pragma mark -
#pragma mark Getters and Setters

- (NSMutableArray*) selectedArray {
    if (!_selectedArray) {
        _selectedArray = [[NSMutableArray alloc] init];
    }
    return _selectedArray;
}

- (NSMutableDictionary*) configuracao {
    if (!_configuracao) {
        AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSURL* storeURL = [[appDelegate applicationDocumentsDirectory] URLByAppendingPathComponent:@"ConfiguracaoMVRestaurante.plist"];
        NSMutableDictionary* config = [[NSMutableDictionary alloc] initWithContentsOfURL:storeURL];
        if (config == nil) {
            config = [[NSMutableDictionary alloc] init];
        }
        _configuracao = config;
    }
    return _configuracao;    
}

#pragma mark -
#pragma mark Funcoes e métodos complementares

NSInteger comparaComplementos(id arg1, id arg2, void *arg3) {
    return [[(Produto*)arg1 nome] compare: [(Produto*)arg2 nome]];
	//Swap arg1 and arg2 to reverse the sort order
}

- (NSUInteger) quantidadeDeComplementosSelecionados {
    return [self.selectedArray count];
}

- (NSUInteger) quantidadeDoComplemento:(id)complemento {
    NSUInteger quantidade = 0;
    for (Produto* compl in self.selectedArray) {
        if (compl == complemento) {
            quantidade++;
        }
    }
    return quantidade;
//    return [[self.selectedArray indexesOfObjectsPassingTest:
//                                       ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
//                                           return obj == complemento;
//                                       }] count];
}

- (void) adicionaComplementoAosSelecionado:(id)complemento {
    if ([self quantidadeDeComplementosSelecionados] < 6) {
        [self.selectedArray addObject:complemento];
    } else {
        [self alertaQuantidadeDeComplementosExcedida];
    }
}

- (void) removeComplementoDosSelecionados:(id)complemento {
    if ([self.selectedArray containsObject:complemento]) {
        NSInteger index = [self.selectedArray indexOfObject:complemento];
        [self.selectedArray removeObjectAtIndex:index];
    }
}

#pragma mark -
#pragma mark Alerts

- (void) alertaQuantidadeDeComplementosExcedida {
	NSString* msg_alert = [NSString stringWithString:@"Só é permitido selecionar até 6 complementos"];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Quantidade de Complementos Excedida"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}

#pragma mark -
#pragma mark Initialization

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[self.configuracao objectForKey:BUSCACOMPLEMENTOSPORNOME_KEY] isEqualToString:@"NO"]) {
        self.sbarBusca.keyboardType = UIKeyboardTypeNumberPad;
    }
	
	self.complementos = [[self.itemSelecionado.produto.categoria.categoria_complemento.produtos allObjects] mutableCopy];
    
    [self.complementos sortUsingFunction:comparaComplementos context:nil];

	self.complementosFiltrados = [NSMutableArray arrayWithCapacity:[self.complementos count]];
//	self.complementosFiltrados = [[NSMutableArray alloc] init];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.title = [NSString stringWithFormat:@"%@", self.mesa];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Concluído" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
//	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)] autorelease];
	
//	if (!self.itemSelecionado.complementosSelecionados) {
//		self.selectedArray = [[NSMutableArray alloc] init];
//	} else {
//		self.selectedArray = [[NSMutableArray alloc] initWithArray:self.itemSelecionado.complementosSelecionados];
//	}
    
    if (self.itemSelecionado.complementosSelecionados) {
        self.selectedArray = self.itemSelecionado.complementosSelecionados;
    }
	
}



- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark -
#pragma mark Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"Complementos";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.complementosFiltrados count];
    } else {
        return [self.complementos count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* CellIdentifier = @"complementoCell";
    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    }
    
    ComplementoTableViewCell* cell = (ComplementoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ComplementoTableViewCell" owner:nil options:nil];
		
	for(id currentObject in topLevelObjects) {			
		if([currentObject isKindOfClass:[ComplementoTableViewCell class]]) {
			cell = (ComplementoTableViewCell *)currentObject;
			break;
		}
	}

    // Configure the cell...

	Produto* complemento = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		complemento = [self.complementosFiltrados objectAtIndex:indexPath.row];
	} else {
		complemento = [self.complementos objectAtIndex:indexPath.row];
	}
	
//	if ([self.selectedArray containsObject:complemento]){
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//	} else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.complementosVC = self;
    cell.complemento = complemento;
	
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
//	ComplementoTableViewCell* cell = (ComplementoTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    	
	Produto* complemento = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		complemento = [self.complementosFiltrados objectAtIndex:indexPath.row];
	} else {
		complemento = [self.complementos objectAtIndex:indexPath.row];
	}	
    
	
//	if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
//		cell.accessoryType = UITableViewCellAccessoryNone;
//		[self.selectedArray removeObject:complemento];
//	} else {
//        if ([self quantidadeDeComplementosSelecionados] < 6) {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            [self.selectedArray addObject:complemento];
//        } else {
//            [self alertaQuantidadeDeComplementosExcedida];
//        }
//	}
    
//    [self adicionaComplementoAosSelecionado:complemento];
//    NSInteger quantidade = [self quantidadeDoComplemento:complemento];
//    cell.lblQuantidade.text = [NSString stringWithFormat:@"%d", quantidade];
	    
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		[self.tbvComplementos reloadData];
		[self.searchDisplayController setActive:NO];
	}
	
	
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
	
	[self.complementosFiltrados removeAllObjects];
	
	for (Produto* complemento in self.complementos) {
        // Busca em qualquer parte da string
        NSString* busca;
        if ([[self.configuracao objectForKey:BUSCACOMPLEMENTOSPORNOME_KEY] isEqualToString:@"NO"]) {
            busca = [complemento.codigo stringValue];
        } else {
            busca = complemento.nome;
        }
        
        if ([busca rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)].location != NSNotFound) {
            //Yes it does contain string
            [self.complementosFiltrados addObject:complemento];
        }

        // Busca no início da string
//        NSComparisonResult result;
//        if ([[self.configuracao objectForKey:BUSCACOMPLEMENTOSPORNOME_KEY] isEqualToString:@"NO"]) {
//            result = [[complemento.codigo stringValue] compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];            
//        } else {
//            result = [complemento.nome compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
//        }
//        
//		if (result == NSOrderedSame)
//		{
//			[self.complementosFiltrados addObject:complemento];
//		}
	}
	
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	
	[self filterContentForSearchText:searchString scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
	
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	[self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
	
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setSbarBusca:nil];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (IBAction) done: (id) sender {
	self.itemSelecionado.complementosSelecionados = self.selectedArray;
    self.menuVC.lblPreco.text = formataNumeroComVirgula([self.itemSelecionado calculaPreco]);
    self.menuVC.lblSubTotal.text = formataNumeroComVirgula([self.menuVC calculaSubtotal]);

	[self.navigationController popToRootViewControllerAnimated:YES];
//	[self dismissModalViewControllerAnimated:YES];
}




@end

