//
//  ItemConta.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 8/19/11.
//  Copyright 2011 MVarandas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"
#import "Conta.h"

@interface ItemConta : Item

@property (nonatomic, strong) Conta* conta;
@property (nonatomic, strong) NSString* codigo;
@property (nonatomic, strong) NSString* sequencia;

@end
