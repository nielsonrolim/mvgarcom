//
//  Conta.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Conta.h"
#import "ItemConta.h"

@implementation Conta

@synthesize items = _items;
@synthesize mesa = _mesa;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (NSMutableArray*) items {
    if (_items == nil) {
        _items = [[NSMutableArray alloc] init];
    }
    return _items;
}

- (BOOL) estaVazio {
    return [self.items count] == 0;
}

- (NSDecimalNumber*) subtotal {
    float subtotal = 0.0;
    
    for (ItemConta* item in self.items) {
        subtotal += [[item calculaSubtotal] floatValue];
    }
    
    return [[NSDecimalNumber alloc] initWithFloat:subtotal];
}

- (NSDecimalNumber*) taxaDeServico {
    float taxaDeServico = 0.0;
    for (ItemConta* item in self.items) {
        if ([item.produto.taxa isEqualToString:@"S"]) {
            taxaDeServico = taxaDeServico + ([[item calculaPreco] floatValue] * 0.10);
        }
    }
    return [[NSDecimalNumber alloc] initWithFloat:taxaDeServico];
}

- (NSDecimalNumber*) total {
    float total = [[self subtotal] floatValue] + [[self taxaDeServico] floatValue];
    return [[NSDecimalNumber alloc] initWithFloat:total];
}

@end
