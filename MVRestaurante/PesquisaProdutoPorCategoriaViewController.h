//
//  PesquisaProdutoPorCategoriaViewController.h
//  MVRestaurant
//
//  Created by Nielson Rolim on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPedido.h"

@class MenuViewController;

@interface PesquisaProdutoPorCategoriaViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UISearchDisplayDelegate, UISearchBarDelegate>

@property (nonatomic, strong) ItemPedido* itemSelecionado;
@property (nonatomic, strong) NSString* mesa;
@property (nonatomic, strong) NSFetchedResultsController* categoriasResultsController;
@property (nonatomic, strong) NSMutableArray* categoriasFiltradas;
@property (nonatomic, strong) IBOutlet UITableView* tbvCategorias;
@property (nonatomic, strong) MenuViewController* menuVC;

@end
