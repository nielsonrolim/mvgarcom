//
//  CancelaItensPorGarcomViewController.m
//  MVRestaurante
//
//  Created by Nielson Rolim on 17/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "CancelaItensPorGarcomViewController.h"
#import "CancelaItensPorGarcomTableViewCell.h"
#import "ItemConta.h"
#import "ItemCancelamento.h"
#import "Obs.h"
#import "Funcoes.h"
#import "SMXMLDocument.h"

@interface CancelaItensPorGarcomViewController ()

@end

@implementation CancelaItensPorGarcomViewController

@synthesize cancelamento = _cancelamento;
@synthesize tbvCancelaPorGarcom = _tbvCancelaPorGarcom;
@synthesize lblMesa = _lblMesa;
@synthesize lblSubtotal = _lblSubtotal;
@synthesize lblTaxaServico = _lblTaxaServico;
@synthesize lblTotal = _lblTotal;
@synthesize wsAtivo = _wsAtivo;
@synthesize viewCarregando = _viewCarregando;
@synthesize lblMsgAguarde = _lblMsgAguarde;
@synthesize celulaSelecionada = _celulaSelecionada;

- (Cancelamento*) cancelamento {
    if (!_cancelamento) {
        _cancelamento = [[Cancelamento alloc] init];
    }
    return _cancelamento;
}

- (NSUInteger) celulaSelecionada {
    if (!_celulaSelecionada) {
        _celulaSelecionada = 0;
    }
    return _celulaSelecionada;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Cancela Itens por Garçom";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.lblMesa.text = [NSString stringWithFormat:@"Mesa %@", self.cancelamento.itemConta.conta.mesa];
    self.lblSubtotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.cancelamento.itemConta.conta subtotal])];
    self.lblTaxaServico.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.cancelamento.itemConta.conta taxaDeServico])];
    self.lblTotal.text = [NSString stringWithFormat:@"R$ %@", formataNumeroComVirgula([self.cancelamento.itemConta.conta total])];
}

- (void)viewDidUnload
{
    [self setTbvCancelaPorGarcom:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.lblMsgAguarde.text = @"Carregando conta";
    self.viewCarregando.hidden = NO;
    
    NSString* paramsStr = [NSString stringWithFormat:@"%@;%@;%@", self.cancelamento.itemConta.conta.mesa, self.cancelamento.itemConta.produto.codigo, self.cancelamento.itemConta.sequencia];
	NSDictionary* params = [NSDictionary dictionaryWithObject:paramsStr forKey:@"REGISTRO"];  
    
	[[RKClient sharedClient] post:@"/le_cancela" params:params delegate:self];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Métodos do cancelamento

- (void) executaCancelaItens {
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.cancelamento.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	ItemCancelamento* itemCancelamento = (ItemCancelamento*)[self.cancelamento.items objectAtIndex:indexPath.row];
	
	if (([itemCancelamento.complementosSelecionados count] > 0) || ([itemCancelamento.observacoesSelecionadas count] > 0)) {
		CGFloat altura;
		altura = 105;
		int linhasAdicionais = [itemCancelamento.complementosSelecionados count] / 5;
		for (int i=0; i < linhasAdicionais; i++) {
			altura += 70;
		}
		return altura;
	} else {
		return 55;
	}
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	ItemCancelamento* itemCancelamento = (ItemCancelamento*)[self.cancelamento.items objectAtIndex:indexPath.row];
	BOOL temComplementos = NO;
	BOOL temObservacoes = NO;
	
	if ([itemCancelamento.observacoesSelecionadas count] > 0) {
		temObservacoes = YES;
	}
	
	if ([itemCancelamento.complementosSelecionados count] > 0) {
		temComplementos = YES;
	}
	
    static NSString* CellIdentifier = @"cancelaItemPorGarcomCell";
	
	CancelaItensPorGarcomTableViewCell* cell = (CancelaItensPorGarcomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	NSArray* topLevelObjects = [NSArray array];
	
	if (temObservacoes || temComplementos) {
		if (temObservacoes && temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensPorGarcomComplObsTableViewCell" owner:nil options:nil];
		} else if (temComplementos) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensPorGarcomComplTableViewCell" owner:nil options:nil];
		} else if (temObservacoes) {
			topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensPorGarcomObsTableViewCell" owner:nil options:nil];
		}
		
	} else {
		topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelaItensPorGarcomTableViewCell" owner:nil options:nil];
	}
	
    
	for(id currentObject in topLevelObjects) {			
		if([currentObject isKindOfClass:[CancelaItensPorGarcomTableViewCell class]]) {
			cell = (CancelaItensPorGarcomTableViewCell *)currentObject;
			break;
		}
	}
	
	cell.item = itemCancelamento;
    cell.linha = indexPath.row;
    cell.lblQuantidade.hidden = NO;
//    NSLog(@"self.celulaSelecionada: %@", self.celulaSelecionada);
//    NSLog(@"cell.linha: %@", indexPath.row);
//    NSLog(@"[cell.item.quantidade intValue]: %@", [cell.item.quantidade intValue]);
    if (self.celulaSelecionada == cell.linha) {
        cell.lblQuantidadeACancelar.hidden = NO;
        cell.btnAdicionaQuantidade.hidden = NO;
        cell.btnSubtraiQuantidade.hidden = NO;
    } else {
        cell.lblQuantidadeACancelar.hidden = YES;
        cell.btnAdicionaQuantidade.hidden = YES;
        cell.btnSubtraiQuantidade.hidden = YES;
    }
	[cell reloadData];
	
	return cell;
    
}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString* CellIdentifier = @"cancelaItemCell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    }
//    
//    
//    return cell;
//}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - RKRequestDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
	
    
	if ([request isPOST]) {
		if ([response isXML]) {
			NSError* error = nil;
			SMXMLDocument* document = [SMXMLDocument documentWithData:[response body] error:&error];
            
            if ([document.root.name isEqualToString:@"Agarcon"]) {
                AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                Garcon* operador = appDelegate.operador;
                NSMutableArray* itensTmp = [[NSMutableArray alloc] init];
                
                for (SMXMLElement* itemXml in [document.root childrenNamed:@"Ggarcon"] ) {
                    
                    ItemCancelamento* itemTmp = [[ItemCancelamento alloc] init];

                    itemTmp.produto = self.cancelamento.itemConta.produto;
                    
                    itemTmp.garcom = [[Garcon alloc] init];
                    itemTmp.garcom.numero = [itemXml valueWithPath:@"codigo"];
                    itemTmp.garcom.nome = [itemXml valueWithPath:@"nome"];
                    
                    itemTmp.quantidade = [NSNumber numberWithDouble:[[itemXml valueWithPath:@"quant"] doubleValue]];
                    
                    [itensTmp addObject:itemTmp];
                    
                }
                self.cancelamento.items = itensTmp;			
                                
                [self.tbvCancelaPorGarcom reloadData];
                
//                self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", [self.conta.items count]];
            } else if ([document.root.value isEqualToString:@"CANCELADO"]) {
//                [self alertaContaCancelada];
            } else if ([document.root.value isEqualToString:@"NAO PERMITIDO"]) {
//                [self alertaNaoPermitido];
            } else if ([document.root.value isEqualToString:@"CONTA FECHADA"]) {
//                [self alertaVermelhoComTitulo:@"Conta Fechada" mensagem:nil];
            } else if ([document.root.value isEqualToString:@"CONTA ABERTA"]) {
//                [self alertaVermelhoComTitulo:@"Mesa Aberta" mensagem:nil];
            } else if ([document.root.value isEqualToString:@"CONTA EMITIDA"]) {
//                [self alertaVermelhoComTitulo:@"Conta Já Emitida" mensagem:nil];
            }
        }
        self.viewCarregando.hidden = YES;
	}
}

- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {
	NSLog(@"INATIVO... Falhou");
	self.wsAtivo = NO;
    //	self.viewCarregando.hidden = YES;
	[self alertaProblemaCancelaItens];
}


#pragma mark - Alertas

- (void) alertaProblemaCancelaItens {
	NSString* msg_alert = [NSString stringWithString:@"Houve um problema ao tentar cancelar itens. Tente novamente."];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Problema no cancelamento"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}


@end
