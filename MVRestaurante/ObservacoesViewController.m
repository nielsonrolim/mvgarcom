//
//  ObservacoesViewController.m
//  MVRestaurant
//
//  Created by Nielson Rolim on 5/17/11.
//  Copyright 2011 ZettaWeb. All rights reserved.
//

#import "AppDelegate.h"
#import "ConstantesConf.h"
#import "ObservacoesViewController.h"
#import "OutrasObservacoesViewController.h"
#import "Obs.h"
#import "Produto.h"
#import "Categoria.h"

@implementation ObservacoesViewController

@synthesize mesa = _mesa;
@synthesize selectedArray = _selectedArray;
@synthesize observacoes = _observacoes;
@synthesize observacoesFiltradas = _observacoesFiltradas;
@synthesize itemSelecionado = _itemSelecionado;
@synthesize tbvObservacoes = _tbvObservacoes;
@synthesize configuracao = _configuracao;
@synthesize sbarBusca = _sbarBusca;

#pragma mark -
#pragma mark Getters and Setters

- (NSMutableArray*) selectedArray {
    if (!_selectedArray) {
        _selectedArray = [[NSMutableArray alloc] init];
    }
    return _selectedArray;
}

- (NSMutableDictionary*) configuracao {
    if (!_configuracao) {
        AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSURL* storeURL = [[appDelegate applicationDocumentsDirectory] URLByAppendingPathComponent:@"ConfiguracaoMVRestaurante.plist"];
        NSMutableDictionary* config = [[NSMutableDictionary alloc] initWithContentsOfURL:storeURL];
        if (config == nil) {
            config = [[NSMutableDictionary alloc] init];
        }
        _configuracao = config;
    }
    return _configuracao;    
}

#pragma mark -
#pragma mark Funcoes e métodos complementares

NSInteger comparaObs(id arg1, id arg2, void *arg3) {
    return [[(Obs *)arg1 descri] compare: [(Obs *)arg2 descri]];
	//Swap arg1 and arg2 to reverse the sort order
}

- (NSUInteger) quantidadeDeObservacoesSelecionados {
    return [self.selectedArray count];
}

- (IBAction) abreOutrasObservacoes:(id)sender {
    OutrasObservacoesViewController* outrasObservacoesVC = [[OutrasObservacoesViewController alloc] initWithNibName:@"OutrasObservacoesViewController" bundle:nil];
	UINavigationController* outrasObservacoesNavCon = [[UINavigationController alloc] initWithRootViewController:outrasObservacoesVC];
    
    outrasObservacoesVC.itemSelecionado = self.itemSelecionado;
    
	[self presentModalViewController:outrasObservacoesNavCon animated:YES];
}

#pragma mark -
#pragma mark Alerts

- (void) alertaQuantidadeDeObservacoesExcedida {
	NSString* msg_alert = [NSString stringWithString:@"Só é permitido selecionar até 6 observações"];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Quantidade de Observações Excedida"
														message:msg_alert
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}

#pragma mark -
#pragma mark View lifecycle

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
	}
	return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[self.configuracao objectForKey:BUSCAOBSERVACOESPORNOME_KEY] isEqualToString:@"NO"]) {
        self.sbarBusca.keyboardType = UIKeyboardTypeNumberPad;
    }

	
//	NSMutableArray* tmpArray = [[NSMutableArray alloc] initWithObjects:@"Observação 1", @"Observação 2", @"Observação 3", @"Observação 4", @"Observação 5", nil];
//	self.observacoes = tmpArray;
//	[tmpArray release];
	
	self.observacoes = [[self.itemSelecionado.produto.categoria.observacoes allObjects] mutableCopy];
    
    [self.observacoes sortUsingFunction:comparaObs context:nil];
	
	self.observacoesFiltradas = [NSMutableArray arrayWithCapacity:[self.observacoes count]];
//	self.observacoesFiltradas = [[NSMutableArray alloc] init];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.title = [NSString stringWithFormat:@"%@", self.mesa];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Concluído" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
	
	if (self.itemSelecionado.observacoesSelecionadas) {
		self.selectedArray = self.itemSelecionado.observacoesSelecionadas;
	}
}

/*
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];	
}
*/

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/

/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"Observações";
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.observacoesFiltradas count];
    } else {
        return [self.observacoes count];
    }
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...	
	Obs* obs = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		obs = [self.observacoesFiltradas objectAtIndex:indexPath.row];
	} else {
		obs = [self.observacoes objectAtIndex:indexPath.row];
	}
	
	if ([self.selectedArray containsObject:obs]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
	} else {
        cell.accessoryType = UITableViewCellAccessoryNone;
	}
	
	cell.textLabel.text = [obs descri];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"Cód.: %@", [obs cod]];
	
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
	
	Obs* obs = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		obs = [self.observacoesFiltradas objectAtIndex:indexPath.row];
	} else {
		obs = [self.observacoes objectAtIndex:indexPath.row];
	}
	
	if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
		cell.accessoryType = UITableViewCellAccessoryNone;
		[self.selectedArray removeObject:obs];
	} else {
        if ([self quantidadeDeObservacoesSelecionados] < 6) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.selectedArray addObject:obs];
        } else {
            [self alertaQuantidadeDeObservacoesExcedida];
        }
	}
	
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		[self.tbvObservacoes reloadData];
		[self.searchDisplayController setActive:NO];
	}
	
	
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
	
	[self.observacoesFiltradas removeAllObjects];
	
	for (Obs* obs in self.observacoes) {
        // Busca em qualquer parte da string
        NSString* busca;
        if ([[self.configuracao objectForKey:BUSCAOBSERVACOESPORNOME_KEY] isEqualToString:@"NO"]) {
            busca = [obs.cod stringValue];
        } else {
            busca = obs.descri;
        }
        
        if ([busca rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)].location != NSNotFound) {
            //Yes it does contain string
            [self.observacoesFiltradas addObject:obs];
        }
        
        // Busca no início da string
        //        NSComparisonResult result;
        //        if ([[self.configuracao objectForKey:BUSCACOMPLEMENTOSPORNOME_KEY] isEqualToString:@"NO"]) {
        //            result = [[complemento.codigo stringValue] compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];            
        //        } else {
        //            result = [complemento.nome compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
        //        }
        //        
        //		if (result == NSOrderedSame)
        //		{
        //			[self.complementosFiltrados addObject:complemento];
        //		}
	}
	
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	
	[self filterContentForSearchText:searchString scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];

    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	[self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
	
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setSbarBusca:nil];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
	self.observacoesFiltradas = nil;
}

- (IBAction) done: (id) sender {
	self.itemSelecionado.observacoesSelecionadas = self.selectedArray;
	
	[self.navigationController popToRootViewControllerAnimated:YES];
//	[self dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}




@end

