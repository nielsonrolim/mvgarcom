//
//  Tbobs.h
//  MVRestauranteCliente
//
//  Created by Nielson Rolim on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tbobs : NSObject

@property (nonatomic, strong) NSNumber* codigo;
@property (nonatomic, strong) NSNumber* categ;
@property (nonatomic, strong) NSString* descri;
@property (nonatomic, strong) NSDecimalNumber* valor;

@end
